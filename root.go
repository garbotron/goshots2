package goshots

import (
	"github.com/gorilla/mux"
	"gitlab.com/garbotron/goshots2/core"
	"gitlab.com/garbotron/goshots2/providers/animeclips"
	"gitlab.com/garbotron/goshots2/providers/animeshots"
	"gitlab.com/garbotron/goshots2/providers/dictionary"
	"gitlab.com/garbotron/goshots2/providers/gamershots"
	"gitlab.com/garbotron/goshots2/providers/jeopardy"
	"gitlab.com/garbotron/goshots2/providers/mtg-art-game"
	"gitlab.com/garbotron/goshots2/providers/namethetune"
	"gitlab.com/garbotron/goshots2/providers/streetview"
	"math/rand"
	"time"
)

func Init(r *mux.Router) error {
	rand.Seed(time.Now().UTC().UnixNano())

	providers := []goshots.Provider{
		&gamershots.Gamershots{},
		&animeshots.Animeshots{},
		&animeclips.AnimeClips{},
		&mtgartgame.MtgArtGame{},
		&streetview.StreetView{},
		&jeopardy.Jeopardy{},
		&dictionary.DictionaryShots{},
		&namethetune.NameTheTune{},
	}

	return goshots.ServerInit(r, providers...)
}
