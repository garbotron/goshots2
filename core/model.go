package goshots

import (
	"time"
)

type FilterType int

const (
	FilterTypeNumber      FilterType = iota
	FilterTypeNumberRange FilterType = iota
	FilterTypeSelectOne   FilterType = iota
	FilterTypeSelectMany  FilterType = iota
)

type FilterValue struct {
	Enabled bool  `json:"enabled"`
	Values  []int `json:"values"`
}

type FilterInfo struct {
	Name          string     `json:"name"`
	Prompt        string     `json:"prompt"`
	Type          FilterType `json:"type"`
	Names         []string   `json:"names"`
	DefaultValues []int      `json:"defaultValues"`
}

type ProviderInfo struct {
	ShortName        string       `json:"shortName"`
	PrettyName       string       `json:"prettyName"`
	Description      []string     `json:"description"`
	Title            string       `json:"title"`
	Prompt           string       `json:"prompt"`
	ElemDetailsTitle string       `json:"elemDetailsTitle"`
	Filters          []FilterInfo `json:"filters"`
}

type ProviderReport struct {
	Providers []ProviderInfo `json:"providers"`
}

const (
	MaxGames                  = 128
	MaxAuthorizedUsersPerGame = 64
)

// a filter selection is saved as a mapping of [filter index] => [values for that filter]
type FilterValues []FilterValue

type GameId string // UUID in the backend
type UserId string // UUID in the backend

type GameListing struct {
	Id   GameId `json:"id"`
	Name string `json:"name"`
}

type GameListings struct {
	Games []GameListing `json:"games"`
}

type NewGameRequest struct {
	Name     string `json:"name"`
	Password string `json:"password"`
}

type AuthorizationRequest struct {
	Password string `json:"password"`
}

type TurnContent struct {
	Content    string `json:"content"`
	Solution   string `json:"solution"`
	DetailsUrl string `json:"detailsUrl"`
}

type Player struct {
	Name         string       `json:"name"`
	EmailAddress string       `json:"email"`
	ProviderName string       `json:"providerName"`
	FilterValues FilterValues `json:"filterValues"`

	// scores for the category chosen by this player
	PerPlayerScores      []int `json:"perPlayerScores"`
	IncorrectGuesses     int   `json:"incorrectGuesses"`
	EverybodyDrinksCount int   `json:"everybodyDrinksCount"`
}

type Game struct {
	Id                 GameId    `json:"id"`
	Name               string    `json:"name"`
	Players            []Player  `json:"players"`
	CurrentPlayerIndex int       `json:"currentPlayerIndex"`
	NumTurns           int       `json:"numTurns"`
	CurrentTurn        int       `json:"currentTurn"`
	LastAccessTime     time.Time `json:"-"`
	PasswordHash       []byte    `json:"-"`
	AuthrorizedUsers   []UserId  `json:"-"`
}

type EmailRecipient struct {
	Address string `json:"address"`
	Name    string `json:"name"`
}

type EmailRequest struct {
	To       []EmailRecipient `json:"to"`
	Solution string           `json:"solution"`
	Link     string           `json:"link"`
}
