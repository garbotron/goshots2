package goshots

import (
	"errors"
)

type Filter interface {
	Name() string
	Prompt() string
	Type() FilterType
	Names(provider Provider) ([]string, error)
	DefaultValues() []int
}

type Provider interface {
	ShortName() string
	PrettyName() string
	Description() []string
	Title() string
	Prompt() string
	ElemDetailsTitle() string
	Filters() []Filter
	Load() error
	RandomElem(filterValues *FilterValues) (interface{}, error)
	ElemSolution(elem interface{}) (string, error)
	RenderContentHtml(elem interface{}) (string, error)
	ElemDetailsUrl(elem interface{}) (string, error)
	CanScrape() bool
	StartScraping(context ScraperContext) Scraper
}

var elemNotFoundError = errors.New("element not found")

func ElemNotFoundError() error {
	return elemNotFoundError
}

func IsElemNotFoundError(err error) bool {
	return (err == elemNotFoundError)
}
