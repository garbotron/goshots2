package goshots

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"github.com/gorilla/mux"
	"github.com/nu7hatch/gouuid"
	"golang.org/x/crypto/bcrypt"
	"net/http"
	"net/smtp"
	"os"
	"time"
)

const userIdCookieName = "goshots-user-id"

func ServerInit(r *mux.Router, providers ...Provider) error {
	for _, p := range providers {
		if err := initProvider(p); err != nil {
			return err
		}
	}

	// /providers: full list of all providers
	r.HandleFunc("/providers", handleProvidersGet).Methods("GET")
	r.HandleFunc("/providers", handleForbidden).Methods("POST", "PUT", "DELETE")

	// /games: full games collection / add new game
	r.HandleFunc("/games", handleGamesGet).Methods("GET")
	r.HandleFunc("/games", handleGamesPost).Methods("POST")
	r.HandleFunc("/games", handleForbidden).Methods("PUT", "DELETE")

	// /games/{id}/authorize: authorize a user to perform other actions
	r.HandleFunc("/games/{id}/authorize", handleAuthorizePost).Methods("POST")

	// /games/{id}: individal game management
	r.HandleFunc("/games/{id}", handleGameGet).Methods("GET")
	r.HandleFunc("/games/{id}", handleGamePut).Methods("PUT")
	r.HandleFunc("/games/{id}", handleGameDelete).Methods("DELETE")
	r.HandleFunc("/games/{id}", handleForbidden).Methods("POST")

	// /games/{id}/content: run the game
	r.HandleFunc("/games/{id}/content", handleContentGet).Methods("GET")
	r.HandleFunc("/games/{id}/content", handleForbidden).Methods("POST", "PUT", "DELETE")

	// /games/{id}/email: send a reminder email to the given users
	r.HandleFunc("/games/{id}/email", handleEmailPost).Methods("POST")

	// handle all other URLs using a simple filesystem-based handler (local "static" directory -> root)
	r.PathPrefix("/").Handler(http.FileServer(http.Dir("static")))

	return nil
}

var allProviders = []Provider{}

func initProvider(p Provider) error {
	if err := p.Load(); err != nil {
		return err
	}
	allProviders = append(allProviders, p)
	return nil
}

func findProvider(name string) (Provider, error) {
	for _, p := range allProviders {
		if p.ShortName() == name {
			return p, nil
		}
	}
	return nil, errors.New("Provider not found")
}

func handleForbidden(w http.ResponseWriter, r *http.Request) {
	http.Error(w, "Invalid operation", http.StatusForbidden)
}

func handleProvidersGet(w http.ResponseWriter, r *http.Request) {
	report := &ProviderReport{[]ProviderInfo{}}
	for _, p := range allProviders {

		filters := []FilterInfo{}
		for _, f := range p.Filters() {
			names, err := f.Names(p)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			filters = append(filters, FilterInfo{
				Name:          f.Name(),
				Prompt:        f.Prompt(),
				Type:          f.Type(),
				Names:         names,
				DefaultValues: f.DefaultValues(),
			})
		}

		report.Providers = append(report.Providers, ProviderInfo{
			ShortName:        p.ShortName(),
			PrettyName:       p.PrettyName(),
			Description:      p.Description(),
			Title:            p.Title(),
			Prompt:           p.Prompt(),
			ElemDetailsTitle: p.ElemDetailsTitle(),
			Filters:          filters,
		})
	}

	outputJson(w, report, http.StatusOK)
}

func handleGamesGet(w http.ResponseWriter, r *http.Request) {
	db, err := OpenDb()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	games, err := db.AllGames()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	data := &GameListings{[]GameListing{}}
	for _, g := range games {
		data.Games = append(data.Games, GameListing{g.Id, g.Name})
	}

	outputJson(w, data, http.StatusOK)
}

func handleGamesPost(w http.ResponseWriter, r *http.Request) {
	db, err := OpenDb()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	inputData := &NewGameRequest{}
	err = readJsonInput(r, inputData)
	if err != nil {
		http.Error(w, "Invalid input", http.StatusForbidden)
		return
	}

	passwordHash, err := bcrypt.GenerateFromPassword([]byte(inputData.Password), bcrypt.DefaultCost)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	userId, err := getUserId(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusForbidden)
		return
	}

	gameId := GameId(createUuid())
	game, err := db.AddGame(gameId, inputData.Name, passwordHash, userId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusForbidden)
	}

	outputJson(w, game, http.StatusOK)
}

func handleAuthorizePost(w http.ResponseWriter, r *http.Request) {
	db, err := OpenDb()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	gameId := mux.Vars(r)["id"]
	game, err := db.FindGame(gameId)
	if err != nil {
		http.Error(w, "Specified game not found", http.StatusNotFound)
		return
	}

	// check if this user is already authorized - if so, we have nothing to do
	if authorizeUser(r, game) == nil {
		w.WriteHeader(http.StatusOK)
		return
	}

	inputData := &AuthorizationRequest{}
	err = readJsonInput(r, inputData)
	if err != nil {
		http.Error(w, "Invalid input", http.StatusForbidden)
		return
	}

	err = bcrypt.CompareHashAndPassword(game.PasswordHash, []byte(inputData.Password))
	if err != nil {
		http.Error(w, "Invalid password", http.StatusForbidden)
		return
	}

	// the password is correct, so add the user to the authorized list
	userId, _ := getUserId(r)
	for len(game.AuthrorizedUsers) >= MaxAuthorizedUsersPerGame {
		game.AuthrorizedUsers = game.AuthrorizedUsers[1:]
	}
	game.AuthrorizedUsers = append([]UserId{userId}, game.AuthrorizedUsers...)
	err = db.UpdateGame(game)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func handleGameGet(w http.ResponseWriter, r *http.Request) {
	db, err := OpenDb()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	gameId := mux.Vars(r)["id"]
	game, err := db.FindGame(gameId)
	if err != nil {
		http.Error(w, "Specified game not found", http.StatusNotFound)
		return
	}

	err = authorizeUser(r, game)
	if err != nil {
		http.Error(w, err.Error(), http.StatusForbidden)
		return
	}

	outputJson(w, game, http.StatusOK)
}

func handleGamePut(w http.ResponseWriter, r *http.Request) {
	db, err := OpenDb()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	gameId := mux.Vars(r)["id"]
	game, err := db.FindGame(gameId)
	if err != nil {
		http.Error(w, "Specified game not found", http.StatusNotFound)
		return
	}

	err = authorizeUser(r, game)
	if err != nil {
		http.Error(w, err.Error(), http.StatusForbidden)
		return
	}

	inputData := &Game{}
	err = readJsonInput(r, inputData)
	if err != nil {
		http.Error(w, "Invalid input: "+err.Error(), http.StatusForbidden)
		return
	}

	game.Name = inputData.Name
	game.CurrentPlayerIndex = inputData.CurrentPlayerIndex
	game.NumTurns = inputData.NumTurns
	game.CurrentTurn = inputData.CurrentTurn
	game.Players = inputData.Players
	err = db.UpdateGame(game)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	outputJson(w, game, http.StatusOK)
}

func handleGameDelete(w http.ResponseWriter, r *http.Request) {
	db, err := OpenDb()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	gameId := mux.Vars(r)["id"]
	game, err := db.FindGame(gameId)
	if err != nil {
		http.Error(w, "Specified game not found", http.StatusNotFound)
		return
	}

	err = authorizeUser(r, game)
	if err != nil {
		http.Error(w, err.Error(), http.StatusForbidden)
		return
	}

	err = db.DeleteGame(game)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func handleContentGet(w http.ResponseWriter, r *http.Request) {
	db, err := OpenDb()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	gameId := mux.Vars(r)["id"]
	game, err := db.FindGame(gameId)
	if err != nil {
		http.Error(w, "Specified game not found", http.StatusNotFound)
		return
	}

	err = authorizeUser(r, game)
	if err != nil {
		http.Error(w, err.Error(), http.StatusForbidden)
		return
	}

	err, content := generateContentOutput(db, game)
	if err != nil {
		if IsElemNotFoundError(err) {
			http.Error(w, err.Error(), http.StatusNotFound)
		} else {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		return
	}

	outputJson(w, content, http.StatusOK)
}

func handleEmailPost(w http.ResponseWriter, r *http.Request) {
	db, err := OpenDb()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	gameId := mux.Vars(r)["id"]
	game, err := db.FindGame(gameId)
	if err != nil {
		http.Error(w, "Specified game not found", http.StatusNotFound)
		return
	}

	err = authorizeUser(r, game)
	if err != nil {
		http.Error(w, err.Error(), http.StatusForbidden)
		return
	}

	inputData := &EmailRequest{}
	err = readJsonInput(r, inputData)
	if err != nil {
		http.Error(w, "Invalid input", http.StatusForbidden)
		return
	}

	server := os.Getenv("GOSHOTS_SMTP_SERVER")
	port := os.Getenv("GOSHOTS_SMTP_PORT")
	user := os.Getenv("GOSHOTS_SMTP_USER")
	password := os.Getenv("GOSHOTS_SMTP_PASSWORD")
	sender := os.Getenv("GOSHOTS_SMTP_SENDER")

	emailAddrs := []string{}
	emailContent := "From: Omnishots <" + sender + ">\nTo: "
	for i, r := range inputData.To {
		if i > 0 {
			emailContent += ", "
		}
		emailContent += r.Name + " <" + r.Address + ">"
		emailAddrs = append(emailAddrs, r.Address)
	}
	emailContent += "\n" +
		"Subject: Omnishots Reminder\n" +
		"Date: " + time.Now().String() + "\n" +
		"Message Body: OMNISHOTS REMINDER\n\n" +
		"You wanted to remember this:\n\n" +
		inputData.Solution + "\n" +
		inputData.Link

	auth := smtp.PlainAuth("", user, password, server)
	err = smtp.SendMail(
		server+":"+port,
		auth,
		sender,
		emailAddrs,
		[]byte(emailContent),
	)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func getUserId(r *http.Request) (UserId, error) {
	userIdCookie, err := r.Cookie(userIdCookieName)
	if err != nil {
		return "", errors.New("User ID cookie not supplied")
	}
	return UserId(userIdCookie.Value), nil
}

func authorizeUser(r *http.Request, game *Game) error {
	userId, err := getUserId(r)
	if err != nil {
		return err
	}
	for _, authorized := range game.AuthrorizedUsers {
		if authorized == userId {
			return nil
		}
	}
	return errors.New("Unauthorized user")
}

func generateContentOutput(db *Db, game *Game) (error, interface{}) {
	if game.CurrentPlayerIndex >= len(game.Players) {
		return errors.New("Invalid current player index"), nil
	}

	provider, err := findProvider(game.Players[game.CurrentPlayerIndex].ProviderName)
	if err != nil {
		return err, nil
	}

	elem, err := provider.RandomElem(&game.Players[game.CurrentPlayerIndex].FilterValues)
	if err != nil {
		return err, nil
	}

	output := &TurnContent{}
	h, err := provider.RenderContentHtml(elem)
	if err != nil {
		return err, nil
	}

	output.Content = base64.StdEncoding.EncodeToString([]byte(h))
	output.Solution, err = provider.ElemSolution(elem)
	if err != nil {
		return err, nil
	}

	output.DetailsUrl, err = provider.ElemDetailsUrl(elem)
	if err != nil {
		return err, nil
	}

	return nil, output
}

func readJsonInput(r *http.Request, data interface{}) error {
	decoder := json.NewDecoder(r.Body)
	return decoder.Decode(data)
}

func outputJson(w http.ResponseWriter, data interface{}, statusCode int) {
	ret, err := json.Marshal(data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(statusCode)
	w.Write(ret)
}

func createUuid() string {
	// for a stupid simple unique key, just use a UUID
	uuid, err := uuid.NewV4()
	if err != nil {
		panic(err.Error())
	}
	return uuid.String()
}
