package goshots

import (
	"errors"
)

type ScraperContext interface {
	Log(fmt string, a ...interface{})
	Error(context string, err error)
	Done(err error)
}

type Scraper interface {
	Abort()
	Progress() (stage string, cur int, total int)
}

var scraperAbortError = errors.New("scrape operation aborted")

func ScraperAbortError() error {
	return scraperAbortError
}

func IsScraperAbortError(err error) bool {
	return (err == scraperAbortError)
}
