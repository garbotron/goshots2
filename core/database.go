package goshots

import (
	"errors"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"os"
	"time"
)

type Db struct {
	db *mgo.Session
}

const (
	MongoDbName              = "" // use the database name from the dial string
	MongoGamesCollectionName = "games"
)

var (
	GameAlreadyExistsError = errors.New("Game already exists")
	TooManyGamesError      = errors.New("Too many games")
	GameNotFoundError      = errors.New("Game not found")
)

func OpenDb() (*Db, error) {
	db, err := mgo.Dial(os.Getenv("GOSHOTS_MONGO_URL"))
	if err != nil {
		return nil, err
	}

	c := db.DB(MongoDbName).C(MongoGamesCollectionName)
	c.EnsureIndexKey("id")

	return &Db{db}, nil
}

func (db *Db) AllGames() ([]*Game, error) {
	c := db.db.DB(MongoDbName).C(MongoGamesCollectionName)
	games := []*Game{}
	if err := c.Find(nil).Sort("name").All(&games); err != nil {
		return nil, err
	}
	return games, nil
}

func (db *Db) FindGame(id string) (*Game, error) {
	c := db.db.DB(MongoDbName).C(MongoGamesCollectionName)
	g := &Game{}
	err := c.Find(bson.M{"id": id}).One(g)
	if err == nil {
		return g, nil
	} else {
		return nil, GameNotFoundError
	}
}

func (db *Db) AddGame(id GameId, name string, passwordHash []byte, creator UserId) (*Game, error) {
	c := db.db.DB(MongoDbName).C(MongoGamesCollectionName)
	count, err := c.Count()
	if err != nil {
		return nil, err
	}
	if count >= MaxGames {
		return nil, TooManyGamesError
	}

	if _, err := db.FindGame(name); err != GameNotFoundError {
		return nil, GameAlreadyExistsError
	}

	game := Game{id, name, []Player{}, 0, 0, 0, time.Now(), passwordHash, []UserId{creator}}
	c.Insert(game)
	return &game, nil
}

func (db *Db) DeleteGame(game *Game) error {
	c := db.db.DB(MongoDbName).C(MongoGamesCollectionName)
	return c.Remove(bson.M{"name": game.Name})
}

func (db *Db) UpdateGame(game *Game) error {
	game.LastAccessTime = time.Now()
	c := db.db.DB(MongoDbName).C(MongoGamesCollectionName)
	return c.Update(bson.M{"name": game.Name}, game)
}
