import { Game, ProviderInfo, ProviderReport, Player } from './model'

export enum Screen {
    Loading,
    GameSelect,
    Content,
    Configuration,
    Stats,
    About,
    Error
}

export interface GameRunner {
    showLoading(text: string): void
    hideLoading(): void

    goToGameSelectScreen(): void
    goToContentScreen(game: Game): void
    goToConfigurationScreen(game: Game): void
    goToStatsScreen(game: Game): void
    goToAboutScreen(game: Game | null): void
    goToErrorScreen(error: string): void

    renderToolbar(game: Game, activeScreen: Screen, extraClass?: string): JSX.Element
    findProvider(shortName: string): ProviderInfo | null
    getProviderAndFilterSummary(player: Player): string
    getProviders(): ProviderReport
}
