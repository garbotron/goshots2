import * as $ from 'jquery'
import * as React from 'react'
import { Game, Player, FilterValue } from '../model'
import { GameRunner, Screen } from '../game-runner'
import { PlayerDetailsEditorComponent } from './player-details-editor'
import Sortable = require('sortablejs')

export interface ConfigurationScreenProps {
    runner: GameRunner
    game: Game
}

export interface ConfigurationScreenState {
    isEditing: boolean
    editPlayerIndex: number
    editPlayerEmail: string
    editPlayerProviderName: string
    editPlayerFilterValues: FilterValue[]
    newPlayerName: string
    newPlayerEmail: string
    newPlayerProviderName: string
}

export class ConfigurationScreenComponent extends React.Component<ConfigurationScreenProps, ConfigurationScreenState> {
    private static getDefaultState(): ConfigurationScreenState {
        return {
            isEditing: false,
            editPlayerIndex: -1,
            editPlayerEmail: '',
            editPlayerProviderName: '',
            editPlayerFilterValues: [],
            newPlayerName: '',
            newPlayerEmail: '',
            newPlayerProviderName: ''
        }
    }

    private menuDropdown: HTMLDivElement | null = null
    private gameLengthDropdown: HTMLDivElement | null = null
    private gameLengthSpan: HTMLSpanElement | null = null
    private newPlayerModal: HTMLDivElement | null = null
    private newPlayerForm: HTMLDivElement | null = null
    private newPlayerProviderDropdown: HTMLDivElement | null = null
    private rearrangeModal: HTMLDivElement | null = null
    private rearrangeList: HTMLOListElement | null = null

    constructor() {
        super()
        this.state = ConfigurationScreenComponent.getDefaultState()
    }

    componentDidMount() {
        $(this.menuDropdown!).dropdown()
        $(this.newPlayerForm!).form({ fields: { name: 'empty', provider: 'empty' } })
        $(this.newPlayerProviderDropdown!).dropdown({
            onChange: (x: string) => {
                let state = this.state
                state.newPlayerProviderName = x
                this.setState(state)
            }
        })
        $(this.gameLengthDropdown!).dropdown({
            onChange: (x: string) => {
                this.props.game.numTurns = parseInt(x)
                $(this.gameLengthSpan!).text(this.getNumTurnsPickerTitle(this.props.game.numTurns))
            }
        })
        if (this.rearrangeList) {
            Sortable.create(this.rearrangeList, {})
        }
    }

    componentDidUpdate() {
        this.componentDidMount()
    }

    render() {
        let menu = this.props.runner.renderToolbar(this.props.game, Screen.Configuration)

        let mainDiv: JSX.Element
        if (this.state.isEditing) {
            mainDiv = (
                <div className='ui left aligned segment'>
                    <PlayerDetailsEditorComponent
                        runner={this.props.runner}
                        email={this.state.editPlayerEmail}
                        providerName={this.state.editPlayerProviderName}
                        filterValues={this.state.editPlayerFilterValues}
                        emailChanged={x => this.editEmailChanged(x)}
                        providerNameChanged={x => this.editProviderNameChanged(x)}
                        filterValuesChanged={x => this.editFilterValuesChanged(x)} />
                    <div className='ui basic right aligned segment' style={{ padding: 0 }}>
                        <button
                            className='ui primary button'
                            style={{ marginTop: '1em' }}
                            onClick={() => this.playerEditApply()}>Apply Changes</button>
                        <button className='ui button' onClick={() => this.playerEditDiscard()}>Discard Changes</button>
                    </div>
                </div>
            )
        } else {
            let turnCounts = [0, 10, 25, 50, 100, 200, 500]
            let turnCountItems = turnCounts.map(
                x => <div key={x} data-value={x} className='item'>{this.getNumTurnsPickerTitle(x)}</div>)

            let providers = this.props.runner.getProviders()
            let providerItems = providers.providers.map(x =>
                <div key={x.shortName} data-value={x.shortName} className='item'>{x.prettyName}</div>)

            let newPlayerModal = (
                <div className='ui small modal' ref={x => this.newPlayerModal = x}>
                    <div className='header'>Add New Player</div>
                    <div className='content'>
                        <div className='ui form' ref={x => this.newPlayerForm = x}>
                            <div className='field'>
                                <label>Player Name</label>
                                <input
                                    type='text'
                                    name='name'
                                    placeholder='Player Name'
                                    onChange={(e: any) => this.setNewPlayerName(e.target.value)}>
                                </input>
                            </div>
                            <div className='two fields'>
                                <div className='field'>
                                    <label>Game Type</label>
                                    <div className='ui selection dropdown' ref={x => this.newPlayerProviderDropdown = x}>
                                        <input name='provider' type='hidden' value={this.state.newPlayerProviderName} />
                                        <i className='dropdown icon'></i>
                                        <div className='default text'>Select Game Type</div>
                                        <div className='menu'>{providerItems}</div>
                                    </div>
                                </div>
                                <div className='field'>
                                    <label>Email Address (optional)</label>
                                    <input
                                        type='email'
                                        name='email'
                                        placeholder='Email Address'
                                        value={this.state.newPlayerEmail}
                                        onChange={(e: any) => this.setNewPlayerEmail(e.target.value)} />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='actions'>
                        <button className='ui positive button'>Configure {this.state.newPlayerName}</button>
                        <button className='ui deny button'>Cancel</button>
                    </div>
                </div>
            )
            
            let rearrangeModal = (
                <div className='ui small modal' ref={x => this.rearrangeModal = x}>
                    <div className='header'>Rearrange Players</div>
                    <div className='content'>
                        <p>Drag and drop to rearrange the player order.</p>
                        <ol className='ui middle aligned relaxed celled ordered omnishots-rearrange list' 
                            ref={x => this.rearrangeList = x}>
                            {this.props.game.players.map((x, i) => <li key={`${x.name}-${i}`} data-id={i} className='item'>
                                <div className='header'>{x.name}</div>
                                <div className='description'>
                                    {this.props.runner.getProviderAndFilterSummary(x)}
                                </div>
                            </li>)}
                        </ol>
                    </div>
                    <div className='actions'>
                        <button className='ui positive button'>Apply</button>
                        <button className='ui deny button'>Cancel</button>
                    </div>
                </div>
            )
            
            let playerRows = this.props.game.players.map((x, i) =>
                <tr key={i}>
                    <td>
                        <h4 className='ui header'>
                            <div className='content'>{x.name}</div>
                            <div className='sub header'>{this.props.runner.getProviderAndFilterSummary(x)}</div>
                        </h4>
                    </td>
                    <td className='right aligned collapsing selectable'>
                        <a href='#' title='Edit Configuration' onClick={e => { e.preventDefault(); this.editPlayerIndex(i); }}>
                            <i className='large edit icon'></i>
                        </a>
                    </td>
                    <td className='right aligned collapsing negative selectable'>
                        <a href='#' title='Delete Player' onClick={e => { e.preventDefault(); this.deletePlayerIndex(i); }}>
                            <i className='large delete icon'></i>
                        </a>
                    </td>
                </tr>
            )

            mainDiv = (
                <div className='ui left aligned clearing segment'>
                    <div className='ui menu'>
                        <div className='ui dropdown item' ref={x => this.gameLengthDropdown = x}>
                            <i className='clock icon'></i>
                            <span className='text' ref={x => this.gameLengthSpan = x }>
                                {this.getNumTurnsPickerTitle(this.props.game.numTurns)}
                            </span>
                            <div className='menu'>{turnCountItems}</div>
                        </div>
                        <div className='right menu'>
                            {newPlayerModal}
                            {rearrangeModal}
                            <a className='item' onClick={() => this.openNewPlayerModal()}>
                                <i className='green user icon'></i>Add Player
                            </a>
                            <div className='ui dropdown link icon item' ref={x => this.menuDropdown = x}>
                                <i className='ellipsis vertical icon'></i>
                                <div className='menu'>
                                    {this.props.game.players.length > 1
                                    ? <a className='item' onClick={() => this.openRearrangeModal()}>
                                        <i className='sort content descending icon'></i>Rearrange Players
                                    </a>
                                    : null}
                                    <a className='item' onClick={() => this.deleteGame()}>
                                        <i className='red remove icon'></i>Delete Game
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table className='ui very compact single line celled table'>
                        <tbody>{playerRows}</tbody>
                    </table>

                    <button
                        className='ui right floated button'
                        onClick={() => this.discardChanges()}>
                        Discard Changes
                    </button>
                    <button
                        className='ui right floated primary button'
                        onClick={() => this.commitChanges()}>
                        Save Changes
                    </button>
                </div>
            )
        }

        return <div className='omnishots-main column'>{menu}{mainDiv}</div>
    }

    private getNumTurnsPickerTitle(turns: number) {
        return 'Game length: ' + (turns == 0 ? 'unlimited' : `${turns} turns`)
    }

    private playerEditApply() {
        this.props.game.players[this.state.editPlayerIndex].email = this.state.editPlayerEmail
        this.props.game.players[this.state.editPlayerIndex].providerName = this.state.editPlayerProviderName
        this.props.game.players[this.state.editPlayerIndex].filterValues = this.state.editPlayerFilterValues

        let state = this.state
        this.state.isEditing = false
        this.setState(state)
    }

    private playerEditDiscard() {
        let state = this.state
        this.state.isEditing = false
        this.setState(state)
    }

    private openNewPlayerModal() {
        $(this.newPlayerModal!).modal({
            onApprove: () => {
                if (!$(this.newPlayerForm!).form('is valid')) {
                    return false
                }
                this.addNewPlayer(this.state.newPlayerName, this.state.newPlayerEmail, this.state.newPlayerProviderName)
                return true
            }
        }).modal('show')
    }

    private openRearrangeModal() {
        $(this.rearrangeModal!).modal({
            onApprove: () => {
                let backup = this.props.game.players.slice(0)
                for (let i = 0; i < this.props.game.players.length; ++i) {
                    let newIdx = $(this.rearrangeList!).children(`[data-id=${i}]`).index()
                    this.props.game.players[newIdx] = backup[i]
                }
                this.setState(ConfigurationScreenComponent.getDefaultState())
                return true
            }
        }).modal('show')
    }

    private setNewPlayerName(name: string) {
        let state = this.state
        state.newPlayerName = name
        this.setState(state)
    }

    private setNewPlayerEmail(email: string) {
        let state = this.state
        state.newPlayerEmail = email
        this.setState(state)
    }

    private addNewPlayer(name: string, email: string, providerName: string) {
        this.props.game.players.push({
            name: name,
            email: email,
            providerName: providerName,
            filterValues: [],
            perPlayerScores: [],
            incorrectGuesses: 0,
            everybodyDrinksCount: 0
        })

        let state = ConfigurationScreenComponent.getDefaultState()
        state.isEditing = true
        state.editPlayerIndex = this.props.game.players.length - 1
        state.editPlayerEmail = email
        state.editPlayerProviderName = providerName
        state.editPlayerFilterValues = []
        this.setState(state)
    }

    private editPlayerIndex(index: number) {
        let state = this.state
        state.isEditing = true
        state.editPlayerIndex = index
        state.editPlayerEmail = this.props.game.players[index].email
        state.editPlayerProviderName = this.props.game.players[index].providerName
        state.editPlayerFilterValues =
            this.props.game.players[index].filterValues.map(x => jQuery.extend(true, {}, x))
        this.setState(state)
    }

    private deletePlayerIndex(index: number) {
        this.props.game.players.splice(index, 1)
        this.setState(ConfigurationScreenComponent.getDefaultState())
    }

    private editEmailChanged(email: string) {
        let state = this.state
        state.editPlayerEmail = email
        this.setState(state)
    }

    private editProviderNameChanged(providerName: string) {
        let state = this.state
        state.editPlayerProviderName = providerName
        this.setState(state)
    }

    private editFilterValuesChanged(values: FilterValue[]) {
        let state = this.state
        state.editPlayerFilterValues = values
        this.setState(state)
    }

    private discardChanges() {
        $.get(`/games/${this.props.game.id}`).done((data: any) => {
            this.props.runner.goToContentScreen(data as Game)
        }).fail((xhr: any, err: any, errorThrown: string) => {
            this.props.runner.goToErrorScreen(`Unable to discard changes: ${errorThrown}`)
        })
        this.props.runner.showLoading('discarding changes')
    }

    private commitChanges() {
        // reset stats since the categories changed
        this.props.game.currentTurn = 0
        for (let player of this.props.game.players) {
            player.perPlayerScores = []
            player.incorrectGuesses = 0
            player.everybodyDrinksCount = 0
        }

        // move on to the next player since the game is being re-configured
        this.props.game.currentPlayerIndex++
        if (this.props.game.currentPlayerIndex >= this.props.game.players.length) {
            this.props.game.currentPlayerIndex = 0
        }

        $.ajax({
            url: `/games/${this.props.game.id}`,
            method: 'PUT',
            data: JSON.stringify(this.props.game)
        }).done((data: any) => {
            this.props.runner.goToContentScreen(data as Game)
        }).fail((xhr: any, error: any, errorThrown: string) => {
            this.props.runner.goToErrorScreen(`Unable to save changes to game: ${errorThrown}`)
        })
        this.props.runner.showLoading('saving changes')
    }

    private deleteGame() {
        $.ajax({
            url: `/games/${this.props.game.id}`,
            method: 'DELETE'
        }).done(() => {
            this.props.runner.goToGameSelectScreen()
        }).fail((xhr: any, error: any, errorThrown: string) => {
            this.props.runner.goToErrorScreen(`Unable to delete game: ${errorThrown}`)
        })
        this.props.runner.showLoading('deleting game')
    }
}
