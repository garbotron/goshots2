import * as $ from 'jquery'
import * as React from 'react'
import { Game, ProviderInfo, ProviderReport, Player, FilterValue, FilterType } from '../model'
import { GameRunner, Screen } from '../game-runner'
import { GameSelectScreenComponent } from './game-select-screen'
import { ConfigurationScreenComponent } from './configuration-screen'
import { ContentScreenComponent } from './content-screen'
import { StatsScreenComponent } from './stats-screen'
import { AboutScreenComponent } from './about-screen'
import { ErrorScreenComponent } from './error-screen'

export interface IndexState {
    screen: Screen
    game: Game | null
    error: string
}

const userIdCookieName = 'omnishots-user-id'
const gameIdCookieName = 'omnishots-game-id'

export class IndexComponent extends React.Component<{}, IndexState> implements GameRunner {
    private providers: ProviderReport | null = null

    constructor() {
        super()
        this.state = { screen: Screen.Loading, game: null, error: '' }
    }

    componentDidMount() {
        // Our user ID cookie must be set up prior to each server API call
        let userId = this.getCookie(userIdCookieName)
        if (userId == '') {
            this.setCookie(userIdCookieName, this.createUuid())
        }

        // Wait until we're done loading providers (other screens will use this info) before loading the initial screen
        if (this.providers == null) {
            $.get('/providers').done((data: any) => {
                this.providers = data as ProviderReport
                this.goToInitialScreen()
            })
            this.showLoading('loading providers')
        } else {
            this.goToInitialScreen()
        }
    }

    render() {
        switch (this.state.screen) {
            case Screen.Loading:
                // loading screen is just blank (we use the loading div overlay via showLoading())
                return <div />
            case Screen.GameSelect:
                return <GameSelectScreenComponent runner={this} />
            case Screen.Configuration:
                return <ConfigurationScreenComponent runner={this} game={this.state.game!} />
            case Screen.Content:
                return <ContentScreenComponent runner={this} game={this.state.game!} />
            case Screen.Stats:
                return <StatsScreenComponent runner={this} game={this.state.game!} />
            case Screen.About:
                return <AboutScreenComponent runner={this} game={this.state.game} />
            case Screen.Error:
                return <ErrorScreenComponent runner={this} error={this.state.error} />
            default:
                return <ErrorScreenComponent runner={this} error='Unknown screen' />
        }
    }

    findProvider(shortName: string): ProviderInfo | null {
        for (let p of this.getProviders().providers) {
            if (p.shortName == shortName) {
                return p
            }
        }
        return null
    }

    getProviders(): ProviderReport {
        if (this.providers) {
            return this.providers
        } else {
            return { providers: [] }
        }
    }

    showLoading(text: string) {
        $('#loading-inner').text(text)
        $('#loading-outer').addClass('active')
    }

    hideLoading() {
        $('#loading-outer').removeClass('active')
    }

    goToGameSelectScreen() {
        this.setCookie(gameIdCookieName, '')
        this.hideLoading()
        this.setState({ screen: Screen.GameSelect, game: null, error: '' })
    }

    goToContentScreen(game: Game) {
        this.setCookie(gameIdCookieName, game.id)
        this.hideLoading()
        if (game.numTurns != 0 && game.currentTurn >= game.numTurns) {
            this.setState({ screen: Screen.Stats, game: game, error: '' })
        } else if (game.players.length == 0) {
            this.setState({ screen: Screen.Configuration, game: game, error: '' })
        } else {
            this.setState({ screen: Screen.Content, game: game, error: '' })
        }
    }

    goToConfigurationScreen(game: Game) {
        this.setCookie(gameIdCookieName, game.id)
        this.hideLoading()
        this.setState({ screen: Screen.Configuration, game: game, error: '' })
    }

    goToStatsScreen(game: Game) {
        this.setCookie(gameIdCookieName, game.id)
        this.hideLoading()
        this.setState({ screen: Screen.Stats, game: game, error: '' })
    }

    goToAboutScreen(game: Game) {
        this.hideLoading()
        this.setState({ screen: Screen.About, game: game, error: '' })
    }

    goToErrorScreen(error: string) {
        this.setCookie(gameIdCookieName, '')
        this.hideLoading()
        this.setState({ screen: Screen.Error, game: null, error: error })
    }

    getProviderAndFilterSummary(player: Player) {
        let provider = this.findProvider(player.providerName)
        let providerName = provider == null ? 'unknown' : provider.prettyName
        let filtersSummary = this.getFiltersSummary(player)
        return (filtersSummary == '') ? providerName : `${providerName} - ${filtersSummary}`
    }

    renderToolbar(game: Game, activeScreen: Screen, extraClass?: string): JSX.Element {
        let renderItem = (screen: Screen, title: string, handler: () => void) => {
            if (screen == activeScreen) {
                return <span className='active item'>{title}</span>
            } else {
                return <a className='item' onClick={handler}>{title}</a>
            }
        }
        let title = game.name
        if (game.numTurns != 0 && game.currentTurn < game.numTurns) {
            title = `${title}: turn ${game.currentTurn + 1} of ${game.numTurns}`
        }
        return (
            <div className={`omnishots-nav-holder ${extraClass}`}>
                <div className='ui inverted compact omnishots-nav segment'>
                    <div className='ui inverted secondary menu'>
                        <span className='header item'>{title}</span>
                        {renderItem(Screen.Content, 'Play', () => this.goToContentScreen(game))}
                        {renderItem(Screen.Configuration, 'Configure', () => this.goToConfigurationScreen(game))}
                        {renderItem(Screen.Stats, 'Stats', () => this.goToStatsScreen(game))}
                        {renderItem(Screen.GameSelect, 'Exit', () => this.goToGameSelectScreen())}
                        {renderItem(Screen.About, 'About', () => this.goToAboutScreen(game))}
                    </div>
                </div>
            </div>
        )
    }

    private getFiltersSummary(player: Player) {
        let provider = this.findProvider(player.providerName)
        if (provider == null) {
            return ''
        }
        let filterSummaries = player.filterValues
            .map((x, i) => this.getFilterSummary(provider!, i, x))
            .filter(x => x != null)
        let ret = filterSummaries.join(', ')
        if (ret.length > 60) {
            ret = `${filterSummaries.length} setting${filterSummaries.length == 1 ? '' : 's'}`
        }
        return ret
    }

    private getFilterSummary(provider: ProviderInfo, filterIdx: number, val: FilterValue) {
        if (!val.enabled || filterIdx >= provider.filters.length) {
            return null
        }

        let filter = provider.filters[filterIdx]
        switch (filter.type) {
            case FilterType.Number:
                return val.values.length == 1 ? `${filter.name}: ${val.values[0]}` : 'ERROR: invalid filter value'
            case FilterType.NumberRange:
                return val.values.length == 2
                    ? `${filter.name}: ${val.values[0]} - ${val.values[1]}`
                    : 'ERROR: invalid filter value'
            case FilterType.One:
                return val.values.length == 1 && val.values[0] < filter.names.length
                    ? `${filter.name}: ${filter.names[val.values[0]]}`
                    : 'ERROR: invalid filter value'
            case FilterType.Many:
                let valNames = val.values.map(x => x < filter.names.length ? filter.names[x] : 'ERROR')
                return `${filter.name}: ${valNames.join(' / ')}`
            default:
                return 'ERROR: invalid filter type'
        }
    }

    private goToInitialScreen() {
        let gameId = this.getCookie(gameIdCookieName)
        if (gameId == '') {
            this.goToGameSelectScreen()
        } else {
            $.get(`/games/${gameId}`).done((data: any) => {
                this.goToContentScreen(data as Game)
            }).fail(() => {
                this.goToGameSelectScreen()
            })
            this.showLoading('loading game data')
        }
    }

    private getCookie(name: string): string {
        name += '='
        for (let c of document.cookie.split(';')) {
            while (c.charAt(0) == ' ') {
                c = c.substring(1)
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length)
            }
        }
        return ''
    }

    private setCookie(name: string, val: string) {
        document.cookie = `${name}=${val}; expires=Fri, 31 Dec 9999 23:59:59 GMT`
    }

    private createUuid(): string {
        // http://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
            let r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        });
    }
}
