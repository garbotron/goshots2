import * as $ from 'jquery'
import * as React from 'react'
import { Game, Player, TurnContent, EmailRequest, EmailRecipient } from '../model'
import { GameRunner, Screen } from '../game-runner'

export interface ContentScreenProps {
    runner: GameRunner
    game: Game
}

export interface ContentScreenState {
    content: TurnContent
    showingSolution: boolean
    showingStealMenu: boolean
    hudHidden: boolean
    emails: { enabled: boolean, address: string }[]
}

export class ContentScreenComponent extends React.Component<ContentScreenProps, ContentScreenState> {
    private emailModal: HTMLDivElement | null = null
    private emailDoneModal: HTMLDivElement | null = null

    constructor() {
        super()
        this.state = {
            content: { content: '', solution: '', detailsUrl: '' },
            showingSolution: false,
            showingStealMenu: false,
            hudHidden: false,
            emails: []
        }
    }

    componentDidMount() {
        document.addEventListener('keyup', this.documentKeyUp)
        this.showNewContent()
    }

    componentWillUnmount() {
        document.removeEventListener('keyup', this.documentKeyUp)
    }

    render() {
        if (this.props.game.currentPlayerIndex >= this.props.game.players.length) {
            return <div>ERROR: BAD CURRENT PLAYER INDEX</div>
        }
        let player = this.props.game.players[this.props.game.currentPlayerIndex]
        let provider = this.props.runner.findProvider(player.providerName)
        if (!provider) {
            return <div>ERROR: BAD PROVIDER NAME</div>
        }

        let visibilityClass = this.state.hudHidden ? 'hidden' : 'visible'
        let menu = this.props.runner.renderToolbar(this.props.game, Screen.Content, visibilityClass)

        let innerHtml = { __html: window.atob(this.state.content.content) }

        let stealMenu: JSX.Element
        if (this.state.showingStealMenu) {
            let items = this.props.game.players
                .filter((x, i) => i != this.props.game.currentPlayerIndex)
                .map((x, i) => {
                    let letter = String.fromCharCode(97 + i)
                    return <button
                        key={i}
                        className={`ui compact button keycode-${65 + i}`}
                        onClick={() => this.submitCorrect(x)}>
                        {`${letter}) ${x.name}`}
                    </button>
            })
            stealMenu = <div>{items}</div>
        } else {
            stealMenu = <div />
        }

        let emailPlayerRows = this.state.emails.map((x, i) =>
            <tr key={i} className={x.enabled ? 'positive' : ''}>
                <td className='middle aligned collapsing'>
                    <div className='ui fitted toggle checkbox'>
                        <input
                            type='checkbox'
                            checked={x.enabled}
                            onChange={(e: any) => {
                                x.enabled = e.target.checked
                                this.forceUpdate()
                            }} />
                        <label />
                    </div>
                </td>
                <td className={x.enabled ?
                    'middle aligned collapsing' : 'middle aligned collapsing disabled'}>
                    {this.props.game.players[i].name}
                </td>
                <td className={x.enabled ?
                    'middle aligned' : 'middle aligned disabled'}>
                    <div className='ui fluid transparent input'>
                        <input
                            type='email'
                            placeholder='email address'
                            value={x.address}
                            onChange={(e: any) => {
                                x.address = e.target.value
                                this.forceUpdate()
                            }} />
                    </div>
                </td>
            </tr>
        )

        let emailModal = (
            <div className='ui small modal' ref={x => this.emailModal = x}>
                <div className='header'>Email these players...</div>
                <div className='content'>
                    <table className='ui compact selectable celled table'>
                        <tbody>{emailPlayerRows}</tbody>
                    </table>
                </div>
                <div className='actions'>
                    <button className='ui positive button'>Send Email</button>
                    <button className='ui deny button'>Cancel</button>
                </div>
            </div>
        )

        let emailDoneModal = (
            <div className='ui small modal' ref={x => this.emailDoneModal = x}>
                <div className='header'>Done!</div>
                <div className='actions'>
                    <button className='ui positive button'>OK</button>
                </div>
            </div>
        )

        return (
            <div className='column' style={{ height: '100%', padding: 0 }}>
                <div className='omnishots-content-holder' dangerouslySetInnerHTML={innerHtml} />
                {menu}
                <div
                    className='omnishots-content-hide keycode-72'
                    title={this.state.hudHidden ? 'Show HUD' : 'Hide HUD'}
                    onClick={() => this.toggleHudVisibility()}>
                    <i className='hide icon' />
                </div>
                {this.state.showingSolution
                    ? <div className={`omnishots-content-solution ${visibilityClass}`}>
                        <div className='ui compact piled segment'>
                            <span dangerouslySetInnerHTML={{ __html: this.state.content.solution }} />
                            <sup>
                                <a href={this.state.content.detailsUrl} title={provider.elemDetailsTitle}>
                                    <i className='info link icon'></i>
                                </a>
                            </sup>
                            <div>
                                <button
                                    className='ui compact positive button keycode-49'
                                    onClick={() => this.submitCorrect(player)}>
                                    1) got it
                                </button>
                                <button
                                    className='ui compact negative button keycode-50'
                                    onClick={() => this.submitIncorrect()}>
                                    2) didn't get it
                                </button>
                                <button
                                    className='ui compact button keycode-51'
                                    onClick={() => this.showNewContent()}>
                                    3) retry
                                </button>
                                <button
                                    className='ui compact button keycode-52'
                                    onClick={() => this.showStealMenu()}>
                                    4) steal
                                </button>
                                <button
                                    className='ui compact button keycode-53'
                                    onClick={() => this.submitEverybodyDrinks()}>
                                    5) everybody drinks
                                </button>
                                {emailModal}
                                {emailDoneModal}
                                <button
                                    className='ui compact button keycode-54'
                                    onClick={() => this.openEmailModal()}>
                                    6) email me!
                                </button>
                            </div>
                            {stealMenu}
                        </div>
                    </div>
                    : <div
                        className={`omnishots-content-prompt keycode-32 ${visibilityClass}`}
                        onClick={() => this.showSolution()}>
                        {player.name}! {provider.prompt}
                    </div>
                }
            </div>
        )
    }

    private documentKeyUp(e: KeyboardEvent) {
        // only handle the standard keybindings if no modals are active
        let modals = $('.modal')
        if (modals.length == 0 || !modals.modal('is active').some((x: any) => x)) {
            $(`.keycode-${e.keyCode}`).click()
        }
    }

    private toggleHudVisibility() {
        let state = this.state
        state.hudHidden = !state.hudHidden
        this.setState(state)
    }

    private getEmails() {
        return this.props.game.players.map((x, i) => {
            return {
                enabled: i == this.props.game.currentPlayerIndex,
                address: x.email
            }
        })
    }

    private showNewContent() {
        $.get(`/games/${this.props.game.id}/content`).done((data: any) => {
            let content = data as TurnContent
            this.setState({
                content: content,
                showingSolution: false,
                showingStealMenu: false,
                hudHidden: false,
                emails: this.getEmails()
            })
            this.props.runner.hideLoading()
        }).fail((xhr: any, error: any, errorThrown: string) => {
            if (xhr.status == 404) {
                let content = this.createNotFoundContent()
                this.setState({
                    content: content,
                    showingSolution: false,
                    showingStealMenu: false,
                    hudHidden: false,
                    emails: this.getEmails()
                })
                this.props.runner.hideLoading()
            } else {
                this.props.runner.goToErrorScreen(`Unable to fetch list of games: ${errorThrown}`)
            }
        })
        this.props.runner.showLoading('loading content')
    }

    private createNotFoundContent() {
        return {
            content: window.btoa('<div class="ui huge negative omnishots-content-not-found message">'
                + 'Nothing was found for this category.<br>'
                + 'Go to the <em>Configure</em> menu to reconfigure the settings.</div>'),
            solution: 'Fix your configuration!',
            detailsUrl: '',
        }
    }

    private submitCorrect(player: Player) {
        // the player got it right, increment the relevant statistic and set the current player index
        let playerIndex = this.props.game.players.indexOf(player)
        if (playerIndex >= 0) {
            this.props.game.currentTurn++
            this.props.game.players[this.props.game.currentPlayerIndex].perPlayerScores[playerIndex] =
                (this.props.game.players[this.props.game.currentPlayerIndex].perPlayerScores[playerIndex] || 0) + 1
            this.props.game.currentPlayerIndex = playerIndex
            this.updateGameAndReload()
        }
    }

    private submitIncorrect() {
        // the player got it wrong, increment the stat and move on to the next player
        this.props.game.currentTurn++
        this.props.game.players[this.props.game.currentPlayerIndex].incorrectGuesses++
        this.props.game.currentPlayerIndex++
        if (this.props.game.currentPlayerIndex >= this.props.game.players.length) {
            this.props.game.currentPlayerIndex = 0
        }
        this.updateGameAndReload()
    }

    private submitEverybodyDrinks() {
        // it's an everybody drinks, so just increment the stat and reload with new content
        this.props.game.currentTurn++
        this.props.game.players[this.props.game.currentPlayerIndex].everybodyDrinksCount++
        this.updateGameAndReload()
    }

    private updateGameAndReload() {
        $.ajax({
            url: `/games/${this.props.game.id}`,
            method: 'PUT',
            data: JSON.stringify(this.props.game),
            success: () => {
                if (this.props.game.numTurns != 0 && this.props.game.currentTurn >= this.props.game.numTurns) {
                    this.props.runner.goToStatsScreen(this.props.game)
                } else {
                    this.showNewContent()
                }
            }
        }).fail((xhr: any, error: any, errorThrown: string) => {
            this.props.runner.goToErrorScreen(`Unable to fetch content: ${errorThrown}`)
        })
        this.props.runner.showLoading('updating game')
    }

    private showSolution() {
        let state = this.state
        state.showingSolution = true
        state.hudHidden = false
        this.setState(state)
    }

    private showStealMenu() {
        let state = this.state
        state.showingStealMenu = true
        state.hudHidden = false
        this.setState(state)
    }

    private openEmailModal() {
        $(this.emailModal!).modal({
            onApprove: () => {
                this.startSendingEmails()
                return true
            }
        }).modal('show')
    }

    private openEmailDoneModal() {
        $(this.emailDoneModal!).modal('show')
    }

    private startSendingEmails() {
        let changed = false;
        let to: EmailRecipient[] = []
        for (let i = 0; i < Math.min(this.props.game.players.length, this.state.emails.length); i++) {
            if (this.state.emails[i].enabled) {
                if (this.props.game.players[i].email != this.state.emails[i].address) {
                    changed = true
                }
                this.props.game.players[i].email = this.state.emails[i].address
                to.push({ address: this.state.emails[i].address, name: this.props.game.players[i].name })
            }
        }
        if (changed) {
            $.ajax({
                url: `/games/${this.props.game.id}`,
                method: 'PUT',
                data: JSON.stringify(this.props.game)
            }).done((data: any) => {
                this.sendEmails(to)
            }).fail((xhr: any, error: any, errorThrown: string) => {
                this.props.runner.goToErrorScreen(`Unable to save email addresses: ${errorThrown}`)
            })
            this.props.runner.showLoading('saving email addresses')
        } else {
            this.sendEmails(to)
        }
    }

    private sendEmails(to: EmailRecipient[]) {
        if (to.length == 0) {
            this.props.runner.hideLoading()
            this.openEmailDoneModal()
            return
        }

        let req: EmailRequest = {
            to: to,
            solution: this.state.content.solution,
            link: this.state.content.detailsUrl
        }

        $.ajax({
            url: `/games/${this.props.game.id}/email`,
            method: 'POST',
            data: JSON.stringify(req)
        }).done((data: any) => {
            this.props.runner.hideLoading()
            this.openEmailDoneModal()
        }).fail((xhr: any, error: any, errorThrown: string) => {
            this.props.runner.goToErrorScreen(`Unable to send email: ${errorThrown}`)
        })
        this.props.runner.showLoading('sending email')
    }
}
