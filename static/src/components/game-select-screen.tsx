import * as $ from 'jquery'
import * as React from 'react'
import * as ReactDOM from 'react-dom'
import { Game, GameListings, NewGameRequest, AuthorizationRequest } from '../model'
import { GameRunner } from '../game-runner'
import '../jquery-plugins'

export interface GameSelectScreenProps { runner: GameRunner }

export interface GameSelectScreenState {
    listings: GameListings

    // note: we can't easily model the game ID for continuing an existing game,
    // since it doesn't play well with semantic dropdowns

    continueName: string
    continuePassword: string
    newGameName: string
    newGamePassword: string
}

export class GameSelectScreenComponent extends React.Component<GameSelectScreenProps, GameSelectScreenState> {
    private tabs: HTMLAnchorElement[] = []
    private dropdown: HTMLDivElement | null = null
    private newGameForm: HTMLFormElement | null = null
    private continueForm: HTMLFormElement | null = null

    constructor() {
        super()
        this.state = {
            listings: { games: [] },
            continueName: '',
            continuePassword: '',
            newGameName: '',
            newGamePassword: ''
        }
    }

    componentDidMount() {
        $(this.dropdown!).dropdown({
            onChange: (x: string) => {
                let state = this.state
                state.continueName = x
                this.setState(state)
            }
        })
        $(this.tabs).tab()
        $(this.newGameForm!).form({ fields: { name: 'empty' } })
        $(this.newGameForm!).on('submit', e => this.handleNewGameSubmit(e))
        $(this.continueForm!).form({ fields: { name: 'empty' } })
        $(this.continueForm!).on('submit', e => this.handleContinueSubmit(e))
        $.get('/games').done((data: any) => {
            let state = this.state
            state.listings = data as GameListings
            this.props.runner.hideLoading()
            this.setState(state)
        }).fail((xhr: any, error: any, errorThrown: string) => {
            this.props.runner.goToErrorScreen(`Unable to fetch list of games: ${errorThrown}`)
        })
        this.props.runner.showLoading('loading games')
    }

    render() {
        let gameItems = this.state.listings.games.map((x, i) =>
            <div key={x.id} data-value={x.id} className='item'>{x.name}</div>)

        return (
            <div className='omnishots-main column' style={{ minHeight: '32em' }}>

            <div className='ui large inverted secondary pointing menu'>
                <a className='active item' data-tab='new' ref={x => this.tabs.push(x)}>New Game</a>
                <a className='item' data-tab='continue' ref={x => this.tabs.push(x)}>Load Game</a>
                <div className='right menu'>
                    <a className='item' onClick={() => this.props.runner.goToAboutScreen(null)}>
                        <i className='info circle icon'></i> omnishots
                    </a>
                </div>
            </div>

            <div className='ui active left aligned clearing tab segment' data-tab='new'>
                <form className='ui form' ref={x => this.newGameForm = x}>
                    <div className='field'>
                        <label>Game Name</label>
                        <input name='name' type='text' onChange={e => this.handleNewGameNameChange(e)} />
                    </div>
                    <div className='field'>
                        <label>Password</label>
                        <input name='password' type='password' onChange={e => this.handleNewGamePasswordChange(e)} />
                    </div>
                    <div className='ui error message' />
                    <button className='ui right floated primary button' type='submit'>Create Game</button>
                </form>
            </div>

            <div className='ui left aligned clearing tab segment' data-tab='continue'>
                <form className='ui form' ref={x => this.continueForm = x}>
                    <div className='field'>
                        <label>Game Name</label>
                        <div className='ui fluid search selection dropdown' ref={x => this.dropdown = x}>
                            <input name='name' type='hidden' />
                            <i className='dropdown icon'></i>
                            <div className='default text'>Select Game</div>
                            <div className='menu'>{gameItems}</div>
                        </div>
                    </div>
                    <div className='field'>
                        <label>
                            Password
                            &nbsp;
                            <i style={{ color: '#888' }}>
                                (can be left blank if this computer has played this game before)
                            </i>
                        </label>
                        <input name='password' type='password' onChange={e => this.handleContinuePasswordChange(e)} />
                    </div>
                    <div className='ui error message' />
                    <button className='ui right floated primary button' type='submit'>Continue Game</button>
                </form>
            </div>

            </div>
        )
    }

    private handleContinuePasswordChange(e: any) {
        let state = this.state
        state.continuePassword = e.target.value
        this.setState(state)
    }

    private handleNewGameNameChange(e: any) {
        let state = this.state
        state.newGameName = e.target.value
        this.setState(state)
    }

    private handleNewGamePasswordChange(e: any) {
        let state = this.state
        state.newGamePassword = e.target.value
        this.setState(state)
    }

    private handleContinueSubmit(e: any) {
        e.preventDefault()
        if (!$(this.continueForm!).form('is valid')) {
            return
        }

        // first try to authorize the user
        let authRequest: AuthorizationRequest = { password: this.state.continuePassword }
        $.post(`/games/${this.state.continueName}/authorize`, JSON.stringify(authRequest)).done(() => {
            // if the user authenticated correctly, get the game data
            $.get(`/games/${this.state.continueName}`).done((data: any) => {
                this.props.runner.goToContentScreen(data as Game)
            }).fail((xhr: any, err: any, errorThrown: string) => {
                this.props.runner.goToErrorScreen(`Unable to fetch data for selected game: ${errorThrown}`)
            })
            this.props.runner.showLoading('loading game data')
        }).fail((xhr: any, error: any, errorThrown: string) => {
            this.props.runner.goToErrorScreen(`Unable to authorize user for selected game: ${errorThrown}`)
        })
        this.props.runner.showLoading('authorizing user')
    }

    private handleNewGameSubmit(e: any) {
        e.preventDefault()
        if (!$(this.newGameForm!).form('is valid')) {
            return
        }

        let data: NewGameRequest = { name: this.state.newGameName, password: this.state.newGamePassword }
        $.post('/games', JSON.stringify(data)).done((data: any) => {
            this.props.runner.goToContentScreen(data as Game)
        }).fail((xhr: any, error: any, errorThrown: string) => {
            this.props.runner.goToErrorScreen(`Unable to create game: ${errorThrown}`)
        })
        this.props.runner.showLoading('creating game')
    }
}
