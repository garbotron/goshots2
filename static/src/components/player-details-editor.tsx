import * as React from 'react'
import { ProviderInfo, FilterInfo, FilterValue, FilterType } from '../model'
import { GameRunner } from '../game-runner'

export interface PlayerDetailsEditorProps {
    runner: GameRunner

    email: string
    providerName: string
    filterValues: FilterValue[]

    emailChanged: (value: string) => void
    providerNameChanged: (value: string) => void
    filterValuesChanged: (values: FilterValue[]) => void
}

export interface PlayerDetailsEditorState {
    editingFilterIndex: number
    displayFilter: string
}

export class PlayerDetailsEditorComponent extends React.Component<PlayerDetailsEditorProps, PlayerDetailsEditorState> {
    private refCallbacks: (() => void)[] = []

    constructor() {
        super()
        this.state = { displayFilter: '', editingFilterIndex: -1 }
    }

    componentDidMount() {
        this.refCallbacks.map(x => x())
    }

    componentDidUpdate() {
        this.refCallbacks.map(x => x())
    }

    render() {
        let providers = this.props.runner.getProviders()
        let selectedProvider = this.props.runner.findProvider(this.props.providerName)
        
        if (!selectedProvider) {
            this.props.runner.goToErrorScreen('Invalid provider')
            return <div />
        }

        let value = this.getSanitizedFilterValues()
        let providerItems = providers.providers.map(x =>
            <div key={x.shortName} data-value={x.shortName} className='item'>{x.prettyName}</div>)

        let getItemClass = (i: number) => {
            if (i == this.state.editingFilterIndex) {
                return 'omnishots-active item'
            } else if (value[i].enabled) {
                return 'omnishots-enabled item'
            } else {
                return 'omnishots-disabled item'
            }
        }

        let getIconClass = (i: number) => value[i].enabled ? 'large check circle icon' : 'large circle icon'

        let filterSelectorItems = selectedProvider.filters.map((x, i) =>
            <div
                key={i}
                className={getItemClass(i)}
                onClick={() => this.filterListClicked(i)}>
                <i className={getIconClass(i)}></i>
                <div className='content'>
                    <div className='header'>{x.name}</div>
                </div>
            </div>
        )

        let providerDropdownRef = this.addRef((x: HTMLDivElement) => {
            $(x).dropdown({
                onChange: (x: string) => {
                    this.props.filterValuesChanged([])
                    this.props.providerNameChanged(x)
                }
            })
        })

        return (
            <div className='ui form' style={{minHeight: '15em'}}>
                <div className='two fields'>
                    <div className='field'>
                        <label>Game Type</label>
                        <div className='ui selection dropdown' ref={providerDropdownRef}>
                            <input type='hidden' value={selectedProvider.shortName} />
                            <i className='dropdown icon'></i>
                            <div className='default text'>Select game type</div>
                            <div className='menu'>{providerItems}</div>
                        </div>
                    </div>
                    <div className='field'>
                        <label>Email Address (optional)</label>
                        <input
                            type='email'
                            name='email'
                            placeholder='Email Address'
                            value={this.props.email}
                            onChange={(e: any) => this.props.emailChanged(e.target.value)} />
                    </div>
                </div>
                <div className='ui grid'>
                    <div className='seven wide column'>
                        <div className='ui middle aligned divided omnishots-filter list'>
                            {filterSelectorItems}
                        </div>
                    </div>
                    {this.renderFilterEditorColumn(selectedProvider, this.state.editingFilterIndex)}
                </div>
            </div>
        )
    }

    private renderFilterEditorColumn(provider: ProviderInfo, index: number) {
        if (provider.filters.length == 0) {
            return <h3 className='ui center aligned disabled header'>(no settings available for this game type)</h3>
        }
        if (index < 0 || index >= provider.filters.length) {
            return <h3 className='ui center aligned disabled header'>please select a setting on the left</h3>
        }

        let onRef = this.addRef((x: HTMLDivElement) => {
            $(x).checkbox({ onChange: () => this.setFilterEnabled(index, !val.enabled) })
        })

        let val = this.getSanitizedFilterValues()[index]
        return (
            // Note: The onChange handler is here just to make react happy.
            // The real work happens in the semantic UI checkbox change handler.
            <div className='nine wide column'>
                <div className='ui toggle checkbox header' ref={onRef}>
                    <input
                        type='checkbox'
                        tabIndex={0}
                        className='hidden'
                        checked={val.enabled}
                        onChange={() => {}} />
                    <label style={val.enabled ? {} : { color: '#808080' }}>{provider.filters[index].name}</label>
                </div>
                {this.renderFilterField1(provider, index)}
                {this.renderFilterField2(provider, index)}
            </div>
        )
    }

    private renderFilterField1(provider: ProviderInfo, index: number) {
        let filter = provider.filters[index]
        let val = this.getSanitizedFilterValues()[index]
        if (!val.enabled) {
            return <div />
        }
        switch (filter.type) {
            case FilterType.Number:
                return (
                    <div className='field'>
                        <label>{filter.prompt}</label>
                        <input
                            type='text'
                            value={val.values[0]}
                            onChange={(e: any) => {
                                let num = +e.target.value
                                if (!isNaN(num)) {
                                    this.setFilterValue(index, 0, num)
                                }
                            }} />
                    </div>
                )
            case FilterType.NumberRange:
                return (
                    <div className='field'>
                        <label>{filter.prompt} between</label>
                        <input
                            type='text'
                            value={val.values[0]}
                            onChange={(e: any) => {
                                let num = +e.target.value
                                if (!isNaN(num)) {
                                    this.setFilterValue(index, 0, num)
                                }
                            }} />
                    </div>
                )
            case FilterType.One:
                let singleItems = filter.names.map((x, i) => <div key={i} data-value={i} className='item'>{x}</div>)
                let singleRef = this.addRef((x: HTMLDivElement) => {
                    $(x).dropdown({
                        onChange: (x: string) => this.setFilterValue(index, 0, +x)
                    })
                })
                return (
                    <div className='ui fluid selection dropdown' ref={singleRef}>
                        <input type='hidden' value={val.values[0]} />
                        <i className='dropdown icon'></i>
                        <div className='default text'>Select a value</div>
                        <div className='menu'>{singleItems}</div>
                    </div>
                )
            case FilterType.Many:
                let indexedFilters = filter.names.map((x, i) => { return { index: i, name: x }})
                let searchField: JSX.Element | null = null
                if (filter.names.length > 25) {
                    // if there are enough entries, add a search filter textbox
                    indexedFilters = indexedFilters.filter(x => {
                        for (let word of this.state.displayFilter.split(' ')) {
                            if (x.name.toLowerCase().indexOf(word.trim().toLowerCase()) < 0) {
                                return false
                            }
                        }
                        return true
                    })
                    searchField = (
                        <div className='field'>
                            <div className='ui fluid icon input'>
                                <input
                                    type='text'
                                    placeholder='Filter...'
                                    value={this.state.displayFilter}
                                    onChange={(e: any) => this.setState({
                                        displayFilter: e.target.value,
                                        editingFilterIndex: this.state.editingFilterIndex
                                    })} />
                                <i className='search icon'></i>
                            </div>
                        </div>
                    )
                }

                let multiItems = indexedFilters.map(x => <option key={x.index} value={x.index}>{x.name}</option>)
                return (
                    <div>
                        {searchField}
                        <select
                            multiple={true}
                            style={{ minHeight: '15em' }}
                            value={val.values.map(x => x.toString())}
                            onChange={(e: any) => this.setFilterValues(index, $(e.target).val().map((x: string) => +x))}>
                            {multiItems}
                        </select>
                    </div>
                )
            default:
                return null
        }
    }

    private renderFilterField2(provider: ProviderInfo, index: number) {
        let filter = provider.filters[index]
        let val = this.getSanitizedFilterValues()[index]
        if (!val.enabled) {
            return <div />
        }
        switch (filter.type) {
            case FilterType.NumberRange:
                return (
                    <div className='field'>
                        <label>and</label>
                        <input
                            type='text'
                            value={val.values[1]}
                            onChange={(e: any) => {
                                let num = +e.target.value
                                if (!isNaN(num)) {
                                    this.setFilterValue(index, 1, num)
                                }
                            }} />
                    </div>
                )
            default:
                return <div />
        }
    }

    private addRef(cb: (x: HTMLDivElement) => void): (x: HTMLDivElement) => void {
        return (x: HTMLDivElement) => this.refCallbacks.push(() => cb(x))
    }

    private setFilterEnabled(index: number, enabled: boolean) {
        let val = this.getSanitizedFilterValues()
        val[index].enabled = enabled
        this.state = { displayFilter: this.state.displayFilter, editingFilterIndex: index }
        this.props.filterValuesChanged(val) // will trigger a re-render
    }

    private setFilterValue(index: number, subIndex: number, value: number) {
        let val = this.getSanitizedFilterValues()
        val[index].values[subIndex] = value
        this.state = { displayFilter: this.state.displayFilter, editingFilterIndex: index }
        this.props.filterValuesChanged(val) // will trigger a re-render
    }

    private setFilterValues(index: number, values: number[]) {
        let val = this.getSanitizedFilterValues()
        val[index].values = values
        this.state = { displayFilter: this.state.displayFilter, editingFilterIndex: index }
        this.props.filterValuesChanged(val) // will trigger a re-render
    }

    private filterListClicked(index: number) {
        // - if you click on a filter that isn't already selected, enable it and select it
        // - if you click on the filter that's already selected, toggle it
        if (this.state.editingFilterIndex == index) {
            this.setFilterEnabled(index, !this.getSanitizedFilterValues()[index].enabled)
        } else {
            this.setFilterEnabled(index, true)
        }
    }

    private getSanitizedFilterValues() {
        let provider = this.props.runner.findProvider(this.props.providerName)
        if (!provider) {
            return [] 
        }

        let ret: FilterValue[] = Array(provider.filters.length)

        // first, copy in the default values for each filter, with the filters disabled
        for (let i = 0; i < ret.length; ++i) {
            ret[i] = { enabled: false, values: provider.filters[i].defaultValues }
        }

        // then copy in the values that are actually set
        // (but make sure we have a valid number of selections for the type)
        for (let i = 0; i < Math.min(ret.length, this.props.filterValues.length); ++i) {
            let isValid: boolean
            switch (provider.filters[i].type) {
                case FilterType.Number:
                case FilterType.One:
                    isValid = (this.props.filterValues[i].values.length == 1)
                    break
                case FilterType.NumberRange:
                    isValid = (this.props.filterValues[i].values.length == 2)
                    break
                default:
                    isValid = true
                break
            }
            if (isValid) {
                ret[i] = this.props.filterValues[i]
            }
        }

        return ret
    }
}

