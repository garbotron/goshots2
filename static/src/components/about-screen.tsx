import * as $ from 'jquery'
import * as React from 'react'
import { GameRunner } from '../game-runner'
import { Game } from '../model'

export interface AboutScreenProps {
    runner: GameRunner
    game: Game | null
}

export class AboutScreenComponent extends React.Component<AboutScreenProps, {}> {
    render() {
        return (
            <div className='omnishots-main column'>
                <h2 className='ui top attached header'>
                    omnishots
                </h2>
                <div className='ui left aligned bottom attached segment'>
                    <p>omnishots is a drinking game for nerds.</p>
                    <p>TODO: details</p>
                    <button
                        className='positive ui button'
                        onClick={() => this.props.game
                            ? this.props.runner.goToContentScreen(this.props.game)
                            : this.props.runner.goToGameSelectScreen() }>
                        Back To The Game
                    </button>
                </div>
            </div>
        )
    }
}
