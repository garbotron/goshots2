import * as $ from 'jquery'
import * as React from 'react'
import { GameRunner } from '../game-runner'

export interface ErrorScreenProps {
    runner: GameRunner
    error: string
}

export class ErrorScreenComponent extends React.Component<ErrorScreenProps, {}> {
    render() {
        return (
            <div className='omnishots-main column'>
                <div className='ui huge negative message'>
                    <div className='header'>An error occurred!</div>
                    <p>{this.props.error}</p>
                    <p>Maybe try reloading?</p>
                </div>
            </div>
        )
    }
}
