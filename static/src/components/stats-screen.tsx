import * as $ from 'jquery'
import * as React from 'react'
import { Game, Player } from '../model'
import { GameRunner, Screen } from '../game-runner'

export interface StatsScreenProps {
    runner: GameRunner
    game: Game
}

export interface StatsScreenState {
    showingPlayerStats: boolean
}

interface PlayerStatsInfo {
    player: Player
    correct: number
    incorrect: number
    steals: number
    totalScore: number
}

interface CategoryStatsInfo {
    player: Player
    gameType: string
    correct: string
    incorrect: string
    steals: string
    everybodyDrinks: string
}

export class StatsScreenComponent extends React.Component<StatsScreenProps, StatsScreenState> {
    constructor() {
        super()
        this.state = { showingPlayerStats: true }
    }

    render() {
        let menu = this.props.runner.renderToolbar(this.props.game, Screen.Stats)

        let playerStats = this.props.game.players.map((x, i) => this.getPlayerStatsInfo(x, i))
        // sort player stats by total score, decending
        let playerStatsSorted = playerStats.sort((x, y) => {
            if (x.totalScore == y.totalScore) {
                return x.player.name.localeCompare(y.player.name)
            } else {
                return y.totalScore - x.totalScore
            }
        })

        let statsTable: JSX.Element
        if (this.state.showingPlayerStats) {
            statsTable = (
                <table className='ui celled definition table'>
                    <thead>
                        <tr>
                            <th></th>
                            <th>Correct</th>
                            <th>Incorrect</th>
                            <th>Steals</th>
                            <th>Total Points</th>
                        </tr>
                    </thead>
                    <tbody>
                        {playerStatsSorted.map(x => this.renderPlayerStatsRow(x))}
                    </tbody>
                </table>
            )
        } else {
            let categoryStats = this.props.game.players.map(x => this.getCategoryStatsInfo(x))
            statsTable = (
                <table className='ui celled definition table'>
                    <thead>
                        <tr>
                            <th></th>
                            <th>Correct</th>
                            <th>Incorrect</th>
                            <th>Steals</th>
                            <th>Everybody Drinks</th>
                        </tr>
                    </thead>
                    <tbody>
                        {categoryStats.map(x => this.renderCategoryStatsRow(x))}
                    </tbody>
                </table>
            )
        }

        let isGameOver = this.props.game.numTurns != 0 && this.props.game.currentTurn >= this.props.game.numTurns
        let winner: JSX.Element | null = null
        if (isGameOver && playerStatsSorted.length > 0) {
            winner = <h2 className='ui green header'>Winner: {playerStatsSorted[0].player.name}</h2>
        }

        let gameOverButtons: JSX.Element | null = null
        if (isGameOver) {
            gameOverButtons = <div className='ui basic segment' style={{ padding: 0 }}>
                <div className='ui button' onClick={() => this.deleteGame()}>Delete Game</div>
                <div className='ui button' onClick={() => this.continueGame()}>Continue This Game (same players)</div>
                <div className='ui button' onClick={() => this.reconfigureGame()}>Reconfigure Game</div>
            </div>
        }

        let mainDiv = (
            <div className='ui segment'>
                {winner}
                <div className='ui massive secondary pointing menu'>
                    <a className={this.state.showingPlayerStats ? 'active item' : 'item'}
                        onClick={() => this.setState({ showingPlayerStats: true})}>
                        <i className='user icon'></i>
                        Player Stats
                    </a>
                    <div className='right menu'>
                        <a className={this.state.showingPlayerStats ? 'item' : 'active item'}
                            onClick={() => this.setState({ showingPlayerStats: false})}>
                            <i className='list layout icon'></i>
                            Category Stats
                        </a>
                    </div>
                </div>
                {statsTable}
                {gameOverButtons}
            </div>
        )
        return <div className='omnishots-main column'>{menu}{mainDiv}</div>
    }

    private getPlayerStatsInfo(player: Player, playerIdx: number): PlayerStatsInfo {
        let ret = { player: player, correct: 0, incorrect: 0, steals: 0, totalScore: 0 }

        // loop over each player's category (including the current player) and tally up the scores
        this.props.game.players.forEach(p => {
            ret.totalScore += p.perPlayerScores[playerIdx] || 0
            if (p == player) {
                ret.correct += p.perPlayerScores[playerIdx] || 0
                ret.incorrect += p.incorrectGuesses
            } else {
                ret.steals += p.perPlayerScores[playerIdx] || 0
            }
        })

        return ret
    }

    private getCategoryStatsInfo(player: Player): CategoryStatsInfo {
        let correct = 0
        let steals = 0

        this.props.game.players.forEach((p, i) => {
            if (p == player) {
                correct += p.perPlayerScores[i] || 0
            } else {
                steals += p.perPlayerScores[i] || 0
            }
        })

        let total = correct + player.incorrectGuesses + steals + player.everybodyDrinksCount
        let percent = (x: number) => ((x * 100) / total).toFixed(0)
        let toStr = (x: number) => x == 0 ? "0" : x + " (" + percent(x) + "%)"
        let provider = this.props.runner.findProvider(player.providerName)
        return {
            player: player,
            gameType: provider ? provider.prettyName : "unknown",
            correct: toStr(correct),
            incorrect: toStr(player.incorrectGuesses),
            steals: toStr(steals),
            everybodyDrinks: toStr(player.everybodyDrinksCount)
        }
    }

    private renderPlayerStatsRow(stats: PlayerStatsInfo) {
        return (
            <tr key={stats.player.name}>
                <td>{stats.player.name}</td>
                <td>{stats.correct}</td>
                <td>{stats.incorrect}</td>
                <td>{stats.steals}</td>
                <td>{stats.totalScore}</td>
            </tr>
        )
    }

    private renderCategoryStatsRow(stats: CategoryStatsInfo) {
        return (
            <tr key={stats.player.name}>
                <td>
                    <h5 className='ui header'>
                        <div className='content'>{stats.player.name}</div>
                        <div className='sub header'>{stats.gameType}</div>
                    </h5>
                </td>
                <td>{stats.correct}</td>
                <td>{stats.incorrect}</td>
                <td>{stats.steals}</td>
                <td>{stats.everybodyDrinks}</td>
            </tr>
        )
    }

    private renderCategoryStats(player: Player, playerIdx: number) {
        let provider = this.props.runner.findProvider(player.providerName)
        if (provider == null) {
            return <h2>ERROR: BAD PROVIDER</h2>
        }

        let numFilters = player.filterValues.filter(x => x.enabled).length
        let playerIndex = this.props.game.players.indexOf(player)

        // resize the array in case it's uninitialized to make it easier to deal with
        while (player.perPlayerScores.length < this.props.game.players.length) {
            player.perPlayerScores.push(0)
        }

        let steals = this.props.game.players.filter(x => x != player).map(x => (
            <li key={this.props.game.players.indexOf(x)}>
                Steals by {x.name}: {player.perPlayerScores[this.props.game.players.indexOf(x)]}
            </li>
        ))

        return (
            <div key={playerIndex} className='ui card'>
                <div className='left aligned content'>
                    <div className='header'>{player.name}'s Category</div>
                    <div className='meta'>{this.props.runner.getProviderAndFilterSummary(player)}</div>
                    <div className='description'>
                        <ul className='ui list'>
                            <li>Everybody drinks: {player.everybodyDrinksCount}</li>
                            <li>Correct guesses: {player.perPlayerScores[playerIndex]}</li>
                            <li>Incorrect guesses: {player.incorrectGuesses}</li>
                            {steals}
                        </ul>
                    </div>
                </div>
            </div>
        )
    }

    private deleteGame() {
        this.props.game.numTurns = 0; // unlimited
        this.props.game.currentTurn = 0;
        $.ajax({
            url: `/games/${this.props.game.id}`,
            method: 'DELETE'
        }).done((data: any) => {
            this.props.runner.goToGameSelectScreen()
        }).fail((xhr: any, error: any, errorThrown: string) => {
            this.props.runner.goToErrorScreen(`Unable to delete game: ${errorThrown}`)
        })
        this.props.runner.showLoading('deleting game')
    }

    private continueGame() {
        this.props.game.numTurns = 0; // unlimited
        this.props.game.currentTurn = 0;
        $.ajax({
            url: `/games/${this.props.game.id}`,
            method: 'PUT',
            data: JSON.stringify(this.props.game)
        }).done((data: any) => {
            this.props.runner.goToContentScreen(data as Game)
        }).fail((xhr: any, error: any, errorThrown: string) => {
            this.props.runner.goToErrorScreen(`Unable to save changes to game: ${errorThrown}`)
        })
        this.props.runner.showLoading('saving changes')
    }

    private reconfigureGame() {
        this.props.runner.goToConfigurationScreen(this.props.game)
    }
}
