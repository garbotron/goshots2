export enum FilterType {
    Number,
    NumberRange,
    One,
    Many
}

export interface FilterInfo {
    name: string
    prompt: string
    type: FilterType
    names: string[]
    defaultValues: number[]
}

export interface ProviderInfo {
    shortName: string
    prettyName: string
    description: string[]
    title: string
    prompt: string
    elemDetailsTitle: string
    filters: FilterInfo[]
}

export interface ProviderReport {
    providers: ProviderInfo[]
}

export interface FilterValue {
    enabled: boolean
    values: number[]
}

export interface GameListing {
    id: string
    name: string
}

export interface GameListings {
    games: GameListing[]
}

export interface NewGameRequest {
    name: string
    password: string
}

export interface AuthorizationRequest {
    password: string
}

export interface TurnContent {
    content: string
    solution: string
    detailsUrl: string
}

export interface Player {
    name: string
    email: string
    providerName: string
    filterValues: FilterValue[]
    perPlayerScores: number[]
    incorrectGuesses: number
    everybodyDrinksCount: number
}

export interface Game {
    id: string
    name: string
    players: Player[]
    currentPlayerIndex: number
    numTurns: number
    currentTurn: number
}

export interface EmailRecipient {
    address: string
    name: string
}

export interface EmailRequest {
    to: EmailRecipient[]
    solution: string
    link: string
}
