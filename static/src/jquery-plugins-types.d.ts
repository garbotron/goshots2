interface JQuery {
    dropdown(data?: any): any
    checkbox(data?: any): any
    tab(data?: any): any
    form(data?: any): any
    modal(data?: any): any
}
