import * as $ from 'jquery'

// some weird stuff to make semantic/typescript/react play nice together
declare var require: any
$.fn.tab = require('semantic-ui-tab')
$.fn.dropdown = require('semantic-ui-dropdown')
$.fn.checkbox = require('semantic-ui-checkbox')
$.fn.form = require('semantic-ui-form')
$.fn.modal = require('semantic-ui-modal')
$.fn.dimmer = require('semantic-ui-dimmer')
$.fn.transition = require('semantic-ui-transition')
