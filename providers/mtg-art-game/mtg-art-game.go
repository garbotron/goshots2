package mtgartgame

import (
	"errors"
	"fmt"
	"gitlab.com/garbotron/goshots2/core"
	"html"
	"io/ioutil"
	"net/http"
	"regexp"
	"strings"
)

type MtgArtGame struct{}

type Card struct {
	Name       string
	Set        string
	ImageUrl   string
	DetailsUrl string
}

func (_ *MtgArtGame) CanScrape() bool {
	return false
}

func (_ *MtgArtGame) StartScraping(context goshots.ScraperContext) goshots.Scraper {
	return nil
}

func (_ *MtgArtGame) ShortName() string {
	return "mtg-art-game"
}

func (_ *MtgArtGame) PrettyName() string {
	return "MtG Art Game"
}

func (_ *MtgArtGame) Description() []string {
	return []string{
		"MtG Art Game is a drinking game for magic dorks.",
		"Check out some card art, see if you can name the card!",
		"<b>If you can't name the card, take a drink!</b>",
		"<b>If you can, everyone else takes a drink and you go again!</b>",
	}
}

func (_ *MtgArtGame) Title() string {
	return "MtG Art Game: Name That Card!"
}

func (_ *MtgArtGame) Prompt() string {
	return "What card is this?"
}

func (_ *MtgArtGame) ElemDetailsTitle() string {
	return "Card Info"
}

func (_ *MtgArtGame) Load() error {
	return nil
}

func (_ *MtgArtGame) RandomElem(filterValues *goshots.FilterValues) (interface{}, error) {
	// The procedure is like this:
	// 1. Download HTML page according to the selected category (easy, normal, etc)
	// 2. Find the pic and save the URL
	// 3. Download the HTML page that accompanies the pic to look up details on the card
	// 4. Return the card info alongside the pic

	// Note that the only filter we support is FilterGameMode.
	// If that filter is not set, we default to impossible mode.
	gameMode := ImpossibleMode
	if len(*filterValues) > 0 && (*filterValues)[0].Enabled && len((*filterValues)[0].Values) > 0 {
		gameMode = GameMode((*filterValues)[0].Values[0])
	}

	mainPage, err := downloadPage(gameMode.Url())
	if err != nil {
		return nil, err
	}

	re, err := regexp.Compile(`<img +src *= *"(.*)/crop/([^"]*)"`)
	if err != nil {
		panic(err.Error())
	}

	match := re.FindStringSubmatch(mainPage)
	if match == nil {
		return nil, errors.New("Card image not found")
	}

	card := &Card{}
	card.ImageUrl = fmt.Sprintf("%s/crop/%s", match[1], match[2])
	card.DetailsUrl = turnImagePathIntoDetailsUrl(match[1], match[2])
	if card.DetailsUrl == "" {
		return nil, errors.New("Couldn't turn image path into details URL")
	}

	detailsPage, err := downloadPage(card.DetailsUrl)
	if err != nil {
		return nil, err
	}

	re, err = regexp.Compile(`<title>(.+) \((.+)\)</title>`)
	if err != nil {
		panic(err.Error())
	}

	match = re.FindStringSubmatch(detailsPage)
	if match == nil {
		return nil, errors.New("Card name/set not found")
	}

	card.Name = html.UnescapeString(match[1])
	card.Set = html.UnescapeString(match[2])
	return card, nil
}

func turnImagePathIntoDetailsUrl(root string, path string) string {
	// We need to turn this: http://magiccards.info/crop/en/un/27.jpg
	// Into this: http://magiccards.info/un/en/27.html
	dirSplit := strings.Split(path, "/")
	if len(dirSplit) != 3 {
		return ""
	}
	fileSplit := strings.Split(dirSplit[2], ".")
	if len(fileSplit) != 2 {
		return ""
	}
	return fmt.Sprintf("%s/%s/%s/%s.html", root, dirSplit[1], dirSplit[0], fileSplit[0])
}

func downloadPage(url string) (string, error) {
	resp, err := http.Get(url)
	if err != nil {
		return "", err
	} else {
		defer resp.Body.Close()
		ret, err := ioutil.ReadAll(resp.Body)
		return string(ret), err
	}
}

func (gs *MtgArtGame) ElemSolution(elem interface{}) (string, error) {
	card := elem.(*Card)
	return fmt.Sprintf("%s (%s)", card.Name, card.Set), nil
}

func (gs *MtgArtGame) RenderContentHtml(elem interface{}) (string, error) {
	card := elem.(*Card)
	str := "<div style=\"width:100%;height:100%;display:table;background-image:url(" +
		card.ImageUrl +
		");background-repeat:no-repeat;background-size:contain;background-position:center;-webkit-background-size:contain;-moz-background-size:contain;-o-background-size:contain;\"> </div>"
	return str, nil
}

func (gs *MtgArtGame) ElemDetailsUrl(elem interface{}) (string, error) {
	card := elem.(*Card)
	return card.DetailsUrl, nil
}
