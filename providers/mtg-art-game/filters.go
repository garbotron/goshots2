package mtgartgame

import (
	"fmt"
	"gitlab.com/garbotron/goshots2/core"
)

type GameMode int

const (
	NewPlayerMode  GameMode = iota
	EasyMode       GameMode = iota
	NormalMode     GameMode = iota
	HardMode       GameMode = iota
	ImpossibleMode GameMode = iota
)

func allGameModes() []GameMode {
	return []GameMode{NewPlayerMode, EasyMode, NormalMode, HardMode, ImpossibleMode}
}

func (m *GameMode) PrettyName() string {
	switch *m {
	case NewPlayerMode:
		return "New Player"
	case EasyMode:
		return "Easy"
	case NormalMode:
		return "Normal"
	case HardMode:
		return "Hard"
	case ImpossibleMode:
		return "Impossible"
	default:
		panic(fmt.Sprintf("UNKNOWN GAME MODE: %d", *m))
	}
}

func (m *GameMode) Url() string {
	switch *m {
	case NewPlayerMode:
		return "http://magiccards.info/art-standard.html"
	case EasyMode:
		return "http://magiccards.info/art-easy.html"
	case NormalMode:
		return "http://magiccards.info/art-normal.html"
	case HardMode:
		return "http://magiccards.info/art-hard.html"
	case ImpossibleMode:
		return "http://magiccards.info/art-impossible.html"
	default:
		panic(fmt.Sprintf("UNKNOWN GAME MODE: %d", *m))
	}
}

func (_ MtgArtGame) Filters() []goshots.Filter {
	return []goshots.Filter{FilterGameMode{}}
}

//----------------------------------------------------------------------------//

type FilterGameMode struct{}

func (_ FilterGameMode) Name() string {
	return "Game Mode"
}

func (_ FilterGameMode) Prompt() string {
	return ""
}

func (_ FilterGameMode) Type() goshots.FilterType {
	return goshots.FilterTypeSelectOne
}

func (_ FilterGameMode) Names(p goshots.Provider) ([]string, error) {
	modes := allGameModes()
	ret := make([]string, len(modes))
	for i := range modes {
		ret[i] = modes[i].PrettyName()
	}
	return ret, nil
}

func (_ FilterGameMode) DefaultValues() []int {
	return []int{int(NormalMode)}
}
