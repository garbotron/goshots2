package streetview

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/garbotron/goshots2/core"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strings"
)

type StreetView struct {
	log *log.Logger
}

type Location struct {
	Name      string
	Latitude  float64
	Longitude float64
}

type googleStreetviewJson struct {
	Result []struct {
		Latitude  float64 `json:"latitude"`
		Longitude float64 `json:"longitude"`
	} `json:"result"`
}

type googleGeocodingJson struct {
	Results []struct {
		AddressComponents []struct {
			LongName string   `json:"long_name"`
			Types    []string `json:"types"`
		} `json:"address_components"`
	} `json:"results"`
}

const (
	maxRandomLocationAttempts          = 250
	googleStreetviewSearchRadiusMeters = 100000
	addressPartCount                   = 2
	enableLogging                      = false
)

func (_ StreetView) Filters() []goshots.Filter {
	return []goshots.Filter{}
}

func (_ *StreetView) CanScrape() bool {
	return false
}

func (_ *StreetView) StartScraping(context goshots.ScraperContext) goshots.Scraper {
	return nil
}

func (_ *StreetView) ShortName() string {
	return "streetview"
}

func (_ *StreetView) PrettyName() string {
	return "StreetView"
}

func (_ *StreetView) Description() []string {
	return []string{
		"StreeView is a drinking game for geography dorks.",
		"Check out a random streetview panorama, see if you can name the location!",
		"<b>If you can't name the location, take a drink!</b>",
		"<b>If you can, everyone else takes a drink and you go again!</b>",
	}
}

func (_ *StreetView) Title() string {
	return "StreetView: Name That Location!"
}

func (_ *StreetView) Prompt() string {
	return "Where is this?"
}

func (_ *StreetView) ElemDetailsTitle() string {
	return "Go to Google Maps"
}

func (s *StreetView) Load() error {
	if enableLogging {
		s.log = log.New(os.Stdout, "[goshots-streetview] ", 0)
	} else {
		s.log = log.New(ioutil.Discard, "", 0)
	}
	return nil
}

func (s *StreetView) RandomElem(filterValues *goshots.FilterValues) (interface{}, error) {
	// The procedure is like this:
	// 1. Pick a random latitude/longitude
	// 2. Query Google StreetView for a nearby street
	// 3. If it comes back with nothing (such as over the ocean), go back to #1
	// 4. Query Google geocoding with the selected coordinates to get the name of the place (and country, etc)

	lat, lng, err := s.getRandomUsableCoordinates()
	if err != nil {
		return nil, s.reportError("get random coordinates", err)
	}

	geocodeData, err := s.queryGoogleGeocoding(lat, lng)
	if err != nil {
		return nil, s.reportError("query geocoding", err)
	}

	if len(geocodeData.Results) == 0 {
		return nil, s.reportErrorString("geocoding data was empty")
	}

	// only the address components of type "political" have useful names
	components := []string{}
	for _, c := range geocodeData.Results[0].AddressComponents {
		if c.Types == nil {
			continue
		}
		isPolitical := false
		for _, x := range c.Types {
			if x == "political" {
				isPolitical = true
			}
		}
		if !isPolitical {
			continue
		}
		components = append(components, c.LongName)
	}

	if len(components) == 0 {
		return nil, s.reportErrorString("geocoding address components were invalid")
	}

	for len(components) > addressPartCount {
		components = components[1:]
	}

	loc := &Location{}
	loc.Latitude = lat
	loc.Longitude = lng
	loc.Name = strings.Join(components, ", ")

	s.log.Printf("success: %f, %f, '%s'\n", loc.Latitude, loc.Longitude, loc.Name)
	return loc, nil
}

func (s *StreetView) ElemSolution(elem interface{}) (string, error) {
	loc := elem.(*Location)
	return loc.Name, nil
}

func (s *StreetView) RenderContentHtml(elem interface{}) (string, error) {
	loc := elem.(*Location)
	return fmt.Sprintf(`<iframe
		src='streetview.html?lat=%f&lng=%f'
		frameborder='0'
		tabindex='-1'
		style='overflow: hidden'
		width='100%'
		height='100%'></iframe>`,
		loc.Latitude,
		loc.Longitude), nil
}

func (s *StreetView) ElemDetailsUrl(elem interface{}) (string, error) {
	loc := elem.(*Location)
	return fmt.Sprintf("https://maps.google.com/?ll=%f,%f", loc.Latitude, loc.Longitude), nil
}

func (s *StreetView) getRandomUsableCoordinates() (lat float64, lng float64, err error) {
	for attempt := 0; attempt < maxRandomLocationAttempts; attempt++ {
		lat := (rand.Float64() * 180.0) - 90
		lng := (rand.Float64() * 360.0) - 180

		s.log.Printf("looking up random coords on streetview: %f, %f\n", lat, lng)

		data, err := s.queryGoogleStreetviewNearestStreet(lat, lng)
		if err != nil {
			return 0, 0, s.reportError("query streetview", err)
		}

		if len(data.Result) > 0 {
			s.log.Printf("found usable coordinates: %f, %f\n", data.Result[0].Latitude, data.Result[0].Longitude)
			return data.Result[0].Latitude, data.Result[0].Longitude, nil
		}
	}

	return 0, 0, s.reportErrorString(
		fmt.Sprintf("Could not find populated place after %d attempts", maxRandomLocationAttempts))
}

func (s *StreetView) queryGoogleStreetviewNearestStreet(lat float64, lng float64) (*googleStreetviewJson, error) {
	url := fmt.Sprintf(
		"https://cbks0.google.com/cbk?cb_client=apiv3&output=polygon&it=1%%3A1&rank=closest&ll=%f,%f&radius=%d",
		lat,
		lng,
		googleStreetviewSearchRadiusMeters)
	data, err := s.downloadPage(url)
	if err != nil {
		return nil, s.reportError("downloadPage", err)
	}

	ret := &googleStreetviewJson{}
	err = json.Unmarshal([]byte(data), ret)
	if err != nil {
		return nil, s.reportError("json.Unmarshal", err)
	}

	return ret, nil
}

func (s *StreetView) queryGoogleGeocoding(lat float64, lng float64) (*googleGeocodingJson, error) {
	url := fmt.Sprintf("https://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f", lat, lng)
	data, err := s.downloadPage(url)
	if err != nil {
		return nil, s.reportError("downloadPage", err)
	}

	ret := &googleGeocodingJson{}
	err = json.Unmarshal([]byte(data), ret)
	if err != nil {
		return nil, s.reportError("json.Unmarshal", err)
	}

	return ret, nil
}

func (s *StreetView) downloadPage(url string) (string, error) {
	resp, err := http.Get(url)
	if err != nil {
		return "", s.reportError("http.Get", err)
	} else {
		defer resp.Body.Close()
		ret, err := ioutil.ReadAll(resp.Body)
		return string(ret), s.reportError("ioutil.ReadAll", err)
	}
}

func (s *StreetView) reportError(action string, err error) error {
	if err == nil {
		return nil
	}
	str := fmt.Sprintf("%s: %s", action, err.Error())
	s.log.Printf("ERROR: %s", str)
	return errors.New(str)
}

func (s *StreetView) reportErrorString(err string) error {
	s.log.Printf("ERROR: %s", err)
	return errors.New(err)
}
