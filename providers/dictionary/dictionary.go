package dictionary

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/garbotron/goshots2/core"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strings"
	"time"
)

type DictionaryShots struct {
	log *log.Logger
}

type WordEntry struct {
	Word    string
	Meaning string
}

const (
	firstYear        = 1999
	monthOfFirstYear = 5
	dayOfFirstMonth  = 3
	enableLogging    = false
)

func (_ DictionaryShots) Filters() []goshots.Filter {
	return []goshots.Filter{}
}

func (_ *DictionaryShots) CanScrape() bool {
	return false
}

func (_ *DictionaryShots) StartScraping(context goshots.ScraperContext) goshots.Scraper {
	return nil
}

func (_ *DictionaryShots) ShortName() string {
	return "dictionary"
}

func (_ *DictionaryShots) PrettyName() string {
	return "DictionaryShots"
}

func (_ *DictionaryShots) Description() []string {
	return []string{
		"StreeViewShots is a drinking game for vocabulary dorks.",
		"You're shown a random English word, see if you know what it means!",
		"<b>If you don't, take a drink!</b>",
		"<b>If you do, everyone else takes a drink and you go again!</b>",
	}
}

func (_ *DictionaryShots) Title() string {
	return "DictionaryShots: What does it mean?!"
}

func (_ *DictionaryShots) Prompt() string {
	return "What does this word mean?"
}

func (_ *DictionaryShots) ElemDetailsTitle() string {
	return "Go to the dictionary entry of the word"
}

func (d *DictionaryShots) Load() error {
	if enableLogging {
		d.log = log.New(os.Stdout, "[goshots-dictionary] ", 0)
	} else {
		d.log = log.New(ioutil.Discard, "", 0)
	}
	return nil
}

func (d *DictionaryShots) RandomElem(filterValued *goshots.FilterValues) (interface{}, error) {
	// Pick a random day/month/year from the beginning of the word-of-the-day until now (one day ago, to be safe)
	oneDayAgo := time.Now().Add(-24 * time.Hour)
	firstDate := time.Date(firstYear, monthOfFirstYear, dayOfFirstMonth, 0, 0, 0, 0, time.UTC)
	numDays := int(oneDayAgo.Sub(firstDate).Hours() / 24)
	randomDayOffset := rand.Intn(numDays)
	date := firstDate.Add(time.Duration(randomDayOffset*24) * time.Hour)

	// Fetch the word-of-the-day JSON for this date
	d.log.Printf("fetching word of the day for %d/%d/%d\n", date.Year(), date.Month(), date.Day())
	url := fmt.Sprintf("http://www.dictionary.com/ajax/wotd/%d/%d/%d", date.Year(), date.Month(), date.Day())
	data, err := d.downloadPage(url)
	if err != nil {
		return nil, err
	}

	var content map[string]interface{}
	err = json.Unmarshal([]byte(data), &content)
	if err != nil {
		return nil, d.reportError("parse JSON data", err)
	}

	contentEntry, ok := content[fmt.Sprintf("%04d-%02d-%02d", date.Year(), date.Month(), date.Day())]
	if !ok {
		return nil, d.reportErrorString("improperly formatted JSON data")
	}

	contentEntryMap := contentEntry.(map[string]interface{})
	wordI, hasWord := contentEntryMap["word"]
	defI, hasDef := contentEntryMap["definition"]
	if !hasWord || !hasDef {
		return nil, d.reportErrorString("improperly formatted JSON data")
	}

	w := &WordEntry{}
	w.Word = wordI.(string)
	w.Meaning = defI.(string)
	w.Meaning = strings.TrimRight(w.Meaning, ".")

	d.log.Printf("success: %s = '%s'\n", w.Word, w.Meaning)
	return w, nil
}

func (d *DictionaryShots) ElemSolution(elem interface{}) (string, error) {
	w := elem.(*WordEntry)
	return w.Meaning, nil
}

func (d *DictionaryShots) RenderContentHtml(elem interface{}) (string, error) {
	w := elem.(*WordEntry)
	str := "<div style=\"width:100%;height:100%;display:flex;justify-content:center;\">" +
		"<div style=\"align-self:center;font-size:600%;color:white;\">" +
		"<strong>\"" + w.Word + "\"</strong>" +
		"</div>"
	return str, nil
}

func (d *DictionaryShots) ElemDetailsUrl(elem interface{}) (string, error) {
	w := elem.(*WordEntry)
	return fmt.Sprintf("http://www.dictionary.com/browse/%s", w.Word), nil
}

func (d *DictionaryShots) downloadPage(url string) (string, error) {
	resp, err := http.Get(url)
	if err != nil {
		return "", d.reportError("http.Get", err)
	} else {
		defer resp.Body.Close()
		ret, err := ioutil.ReadAll(resp.Body)
		return string(ret), d.reportError("ioutil.ReadAll", err)
	}
}

func (d *DictionaryShots) reportError(action string, err error) error {
	if err == nil {
		return nil
	}
	str := fmt.Sprintf("%s: %s", action, err.Error())
	d.log.Printf("ERROR: %s", str)
	return errors.New(str)
}

func (d *DictionaryShots) reportErrorString(err string) error {
	d.log.Printf("ERROR: %s", err)
	return errors.New(err)
}
