package animeclips

import (
	"errors"
	"fmt"
	"gitlab.com/garbotron/goshots2/core"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"math/rand"
	"net/url"
	"os"
)

const LocalServerPort = 8119
const MinStartDelaySecs = 15
const MongoDbName = "" // use the database name from the dial string
const MongoCollectionName = "animeclips"

type AnimeClips struct {
	db *mgo.Session
}

type Clip struct {
	Name         string "name"
	Filename     string "filename"
	DurationSecs int    "duration_secs"
	Start        int
	Length       int
}

func (_ *AnimeClips) CanScrape() bool {
	return false
}

func (_ *AnimeClips) StartScraping(context goshots.ScraperContext) goshots.Scraper {
	return nil
}

func (_ *AnimeClips) ShortName() string {
	return "animeclips"
}

func (_ *AnimeClips) PrettyName() string {
	return "Anime Openings (requires client tools)"
}

func (_ *AnimeClips) Description() []string {
	return []string{
		"Animeclips is a drinking game for anime dorks.",
		"Check out a bit of an opening, see if you can name the show!",
		"<b>If you can't name the show, take a drink!</b>",
		"<b>If you can, everyone else takes a drink and you go again!</b>",
	}
}

func (_ *AnimeClips) Title() string {
	return "Animeclips: Name That Show!"
}

func (_ *AnimeClips) Prompt() string {
	return "What show is this?"
}

func (_ *AnimeClips) ElemDetailsTitle() string {
	return "Full opening"
}

func (ac *AnimeClips) Load() error {
	if ac.db != nil {
		ac.db.Close()
	}
	var err error
	ac.db, err = mgo.Dial(os.Getenv("ANIMECLIPS_MONGO_URL"))
	return err
}

func (as *AnimeClips) RandomElem(filterValues *goshots.FilterValues) (interface{}, error) {
	db := as.db.DB(MongoDbName).C(MongoCollectionName)

	objIds := []struct {
		ID bson.ObjectId "_id"
	}{}

	err := db.Find(nil).Select(bson.M{"_id": 1}).All(&objIds)
	if err != nil {
		return nil, err
	}

	if len(objIds) == 0 {
		return nil, goshots.ElemNotFoundError()
	}

	elemIdx := rand.Int() % len(objIds)
	clip := &Clip{}
	err = db.FindId(objIds[elemIdx].ID).One(&clip)
	if err != nil {
		return "", err
	}

	// Note that the only filter we support is FilterClipLength.
	// If that filter is not set, we default to 6 seconds.
	clipLenSecs := 6
	if len(*filterValues) > 0 && (*filterValues)[0].Enabled && len((*filterValues)[0].Values) > 0 {
		clipLenSecs = (*filterValues)[0].Values[0]
	}

	earliest := MinStartDelaySecs
	latest := clip.DurationSecs - clipLenSecs

	if earliest >= latest {
		return "", errors.New("Clip was too short")
	}

	clip.Start = earliest + (rand.Int() % (latest - earliest))
	clip.Length = clipLenSecs
	return clip, nil
}

func (gs *AnimeClips) ElemSolution(elem interface{}) (string, error) {
	clip := elem.(*Clip)
	return clip.Name, nil
}

func (gs *AnimeClips) RenderContentHtml(elem interface{}) (string, error) {
	clip := elem.(*Clip)
	return fmt.Sprintf(
		`<video poster='video-loading.gif' style='width: 100%%; height: 100%%;' controls preload autoplay>`+
			`<source src='http://localhost:%d/%s?start=%d&len=%d' type='video/mp4'>`+
			`</video>`,
		LocalServerPort,
		url.QueryEscape(clip.Filename),
		clip.Start,
		clip.Length), nil
}

func (gs *AnimeClips) ElemDetailsUrl(elem interface{}) (string, error) {
	clip := elem.(*Clip)
	return fmt.Sprintf("animeclip.html?port=%d&file=%s", LocalServerPort, url.QueryEscape(clip.Filename)), nil
}
