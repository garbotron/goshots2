package animeclips

import (
	"gitlab.com/garbotron/goshots2/core"
)

func (_ AnimeClips) Filters() []goshots.Filter {
	return []goshots.Filter{FilterClipLength{}}
}

type FilterClipLength struct{}

func (_ FilterClipLength) Name() string {
	return "Clip Length"
}

func (_ FilterClipLength) Prompt() string {
	return "Clip length in seconds"
}

func (_ FilterClipLength) Type() goshots.FilterType {
	return goshots.FilterTypeNumber
}

func (_ FilterClipLength) Names(p goshots.Provider) ([]string, error) {
	return nil, nil
}

func (_ FilterClipLength) DefaultValues() []int {
	return []int{6}
}
