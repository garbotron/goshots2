package jeopardy

import (
	"gitlab.com/garbotron/goshots2/core"
)

// This project doesn't expose its own scraper for now - it uses a custom branch of the
// excellent J!Party crawler: https://gitlab.com/garbotron/goshots2-j-party-crawler
func (_ *Jeopardy) CanScrape() bool {
	return false
}

func (_ *Jeopardy) StartScraping(context goshots.ScraperContext) goshots.Scraper {
	return nil
}
