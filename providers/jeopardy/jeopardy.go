package jeopardy

import (
	"database/sql"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/garbotron/goshots2/core"
	"html"
	"strings"
)

const SqliteFileName = "providers/jeopardy/clues.db"
const CluesTableName = "clues"
const CategoriesTableName = "categories"
const SourcesTableName = "sources"

type Jeopardy struct {
	db *sql.DB
}

func (_ *Jeopardy) ShortName() string {
	return "jeopardy"
}

func (_ *Jeopardy) PrettyName() string {
	return "Jeopardy!"
}

func (_ *Jeopardy) Description() []string {
	return []string{
		"Jeopardyshots is a drinking game for trivia geeks.",
		"Can you get a Jeopardy! question right from all of the show's history?",
		"<b>If you can't get it, take a drink!</b>",
		"<b>If you can, everyone else takes a drink and you go again!</b>",
	}
}

func (_ *Jeopardy) Title() string {
	return "Jeopardyshots! Answer in the Form of a Question!"
}

func (_ *Jeopardy) Prompt() string {
	return "Answer in the form of a question."
}

func (_ *Jeopardy) ElemDetailsTitle() string {
	return "Episode Info"
}

func (j *Jeopardy) Load() error {
	var err error
	j.db, err = sql.Open("sqlite3", SqliteFileName)
	return err
}

func (j *Jeopardy) RandomElem(filterValues *goshots.FilterValues) (interface{}, error) {
	filters := JeopardyFilters()
	conditions := []string{}
	for i, fv := range *filterValues {
		if fv.Enabled {
			conditions = append(conditions, filters[i].Apply(j, fv.Values))
		}
	}

	query := fmt.Sprintf(
		"SELECT a.id FROM %s a JOIN %s b ON a.sourceId = b.id",
		CluesTableName,
		SourcesTableName)
	if len(conditions) > 0 {
		query = fmt.Sprintf("%s WHERE %s", query, strings.Join(conditions, " AND "))
	}
	query = fmt.Sprintf("%s ORDER BY RANDOM() LIMIT 1", query)

	var id int
	err := j.db.QueryRow(query).Scan(&id)
	switch {
	case err == sql.ErrNoRows:
		return nil, goshots.ElemNotFoundError()
	case err != nil:
		return nil, err
	default:
		return id, nil
	}
}

func (j *Jeopardy) ElemSolution(elem interface{}) (string, error) {
	id := elem.(int)
	query := fmt.Sprintf("SELECT answer FROM %s WHERE id = %d", CluesTableName, id)
	var answer string
	err := j.db.QueryRow(query).Scan(&answer)
	if err != nil {
		return "", err
	}
	return answer, nil
}

func (j *Jeopardy) RenderContentHtml(elem interface{}) (string, error) {
	id := elem.(int)
	query := fmt.Sprintf(
		"SELECT a.level, a.clue, b.name, c.uri, c.year "+
			"FROM %s a "+
			"JOIN %s b ON a.categoryId = b.id "+
			"JOIN %s c ON b.sourceId = c.id "+
			"WHERE a.id = %d",
		CluesTableName,
		CategoriesTableName,
		SourcesTableName,
		id)
	var level int
	var clue string
	var categoryName string
	var sourceUri string
	var sourceYear int
	err := j.db.QueryRow(query).Scan(&level, &clue, &categoryName, &sourceUri, &sourceYear)
	if err != nil {
		return "", err
	}

	categoryName = strings.TrimSpace(categoryName)
	categoryName = html.EscapeString(categoryName)

	clue = strings.TrimSpace(clue)
	clue = html.EscapeString(clue)
	clue = strings.Replace(clue, "\r", "", -1)
	clue = strings.Replace(clue, "\n", "<br>", -1)

	var difficulty string
	if level == 999 {
		difficulty = "Final Jeopardy!"
	} else {
		difficulty = fmt.Sprintf("Difficulty %d", level+1)
	}

	str := fmt.Sprintf("<div style='width:100%%;height:100%%;background-color:#0A1981;display:flex;justify-content:center;'>"+
		"<div style='align-self:center;margin:5em;font-family:Serif;color:white;font-size:350%%;line-height:1em;text-shadow:0.05em 0.05em 0.02em #111;'>"+
		"<strong>%s</strong><br>%d - %s<br><br><br>%s</div></div>",
		categoryName, sourceYear, difficulty, clue)
	return str, nil
}

func (j *Jeopardy) ElemDetailsUrl(elem interface{}) (string, error) {
	id := elem.(int)
	query := fmt.Sprintf(
		"SELECT b.uri FROM %s a JOIN %s b ON a.sourceId = b.id WHERE a.id = %d",
		CluesTableName,
		SourcesTableName,
		id)
	var uri string
	err := j.db.QueryRow(query).Scan(&uri)
	if err != nil {
		return "", err
	}
	return uri, nil
}
