package jeopardy

import (
	"fmt"
	"gitlab.com/garbotron/goshots2/core"
	"time"
)

type JeopardyFilter interface {
	goshots.Filter
	Apply(j *Jeopardy, vals []int) string
}

func JeopardyFilters() []JeopardyFilter {
	return []JeopardyFilter{
		jFilterDifficulty{},
		jFilterYear{}}
}

func (_ Jeopardy) Filters() []goshots.Filter {
	filters := JeopardyFilters()
	ret := make([]goshots.Filter, len(filters))
	for i := 0; i < len(filters); i++ {
		ret[i] = filters[i]
	}
	return ret
}

//----------------------------------------------------------------------------//

type jFilterDifficulty struct{}

func (_ jFilterDifficulty) Name() string {
	return "Filter by Difficulty"
}

func (_ jFilterDifficulty) Prompt() string {
	return "Clue difficulty 1-5 (6 for Final Jeopardy!)"
}

func (_ jFilterDifficulty) Type() goshots.FilterType {
	return goshots.FilterTypeNumberRange
}

func (_ jFilterDifficulty) Names(p goshots.Provider) ([]string, error) {
	return []string{}, nil
}

func (_ jFilterDifficulty) DefaultValues() []int {
	return []int{1, 5}
}

func (_ jFilterDifficulty) Apply(j *Jeopardy, vals []int) string {
	if len(vals) < 2 {
		return "0 = 1" // false
	}

	// in the DB, difficulties are 0..4 and 999 but we're using 1..5 and 6
	min := vals[0] - 1
	max := vals[1] - 1
	includeFinal := false
	if max >= 4 {
		max = 4
		includeFinal = true
	}
	ret := fmt.Sprintf("level >= %d AND level <= %d", min, max)
	if includeFinal {
		ret = fmt.Sprintf("((level = 999) OR (%s))", ret)
	}
	return ret
}

//----------------------------------------------------------------------------//

type jFilterYear struct{}

func (_ jFilterYear) Name() string {
	return "Filter by Year"
}

func (_ jFilterYear) Prompt() string {
	return "Year when episode aired"
}

func (_ jFilterYear) Type() goshots.FilterType {
	return goshots.FilterTypeNumberRange
}

func (_ jFilterYear) Names(p goshots.Provider) ([]string, error) {
	return []string{}, nil
}

func (_ jFilterYear) DefaultValues() []int {
	return []int{1984, time.Now().Year()}
}

func (_ jFilterYear) Apply(j *Jeopardy, vals []int) string {
	if len(vals) < 2 {
		return "0 = 1" // false
	}
	return fmt.Sprintf("year >= %d AND year <= %d", vals[0], vals[1])
}
