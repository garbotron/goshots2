package namethetune

import (
	"bufio"
	"gitlab.com/garbotron/goshots2/core"
	"os"
	"path"
)

func (_ NameTheTune) Filters() []goshots.Filter {
	return []goshots.Filter{
		NameTheTuneFilterGenre{},
		NameTheTuneFilterClipLengthSecs{},
		NameTheTuneFilterArtistsPerGenre{},
		NameTheTuneFilterArtistsTotal{},
		NameTheTuneFilterSongsPerArtist{}}
}

//----------------------------------------------------------------------------//

type NameTheTuneFilterGenre struct{}

func (_ NameTheTuneFilterGenre) Name() string {
	return "Choose Genre"
}

func (_ NameTheTuneFilterGenre) Prompt() string {
	return ""
}

func (_ NameTheTuneFilterGenre) Type() goshots.FilterType {
	return goshots.FilterTypeSelectMany
}

func (_ NameTheTuneFilterGenre) Names(p goshots.Provider) ([]string, error) {
	// get the list of genres from 'genres.lst' in the same directory as this source file
	lstFile, err := os.Open(path.Join("providers", "namethetune", "genres.lst"))
	if err != nil {
		return []string{}, err
	}

	defer lstFile.Close()
	scanner := bufio.NewScanner(lstFile)
	scanner.Split(bufio.ScanLines)
	genres := []string{}
	for scanner.Scan() {
		genres = append(genres, scanner.Text())
	}
	return genres, nil
}

func (_ NameTheTuneFilterGenre) DefaultValues() []int {
	return []int{}
}

//----------------------------------------------------------------------------//

type NameTheTuneFilterClipLengthSecs struct{}

const NameTheTuneFilterClipLengthSecsDefault = 6

func (_ NameTheTuneFilterClipLengthSecs) Name() string {
	return "Clip Length"
}

func (_ NameTheTuneFilterClipLengthSecs) Prompt() string {
	return "Clip length in seconds"
}

func (_ NameTheTuneFilterClipLengthSecs) Type() goshots.FilterType {
	return goshots.FilterTypeNumber
}

func (_ NameTheTuneFilterClipLengthSecs) Names(p goshots.Provider) ([]string, error) {
	return []string{}, nil
}

func (_ NameTheTuneFilterClipLengthSecs) DefaultValues() []int {
	return []int{NameTheTuneFilterClipLengthSecsDefault}
}

//----------------------------------------------------------------------------//

type NameTheTuneFilterArtistsPerGenre struct{}

const NameTheTuneFilterArtistsPerGenreDefault = 50

func (_ NameTheTuneFilterArtistsPerGenre) Name() string {
	return "Artists per Genre"
}

func (_ NameTheTuneFilterArtistsPerGenre) Prompt() string {
	return "Consider this many artists for each genre"
}

func (_ NameTheTuneFilterArtistsPerGenre) Type() goshots.FilterType {
	return goshots.FilterTypeNumber
}

func (_ NameTheTuneFilterArtistsPerGenre) Names(p goshots.Provider) ([]string, error) {
	return []string{}, nil
}

func (_ NameTheTuneFilterArtistsPerGenre) DefaultValues() []int {
	return []int{NameTheTuneFilterArtistsPerGenreDefault}
}

//----------------------------------------------------------------------------//

type NameTheTuneFilterArtistsTotal struct{}

const NameTheTuneFilterArtistsTotalDefault = 100

func (_ NameTheTuneFilterArtistsTotal) Name() string {
	return "Max Number of Artists"
}

func (_ NameTheTuneFilterArtistsTotal) Prompt() string {
	return "Maximum number of artists to consider (total)"
}

func (_ NameTheTuneFilterArtistsTotal) Type() goshots.FilterType {
	return goshots.FilterTypeNumber
}

func (_ NameTheTuneFilterArtistsTotal) Names(p goshots.Provider) ([]string, error) {
	return []string{}, nil
}

func (_ NameTheTuneFilterArtistsTotal) DefaultValues() []int {
	return []int{NameTheTuneFilterArtistsTotalDefault}
}

//----------------------------------------------------------------------------//

type NameTheTuneFilterSongsPerArtist struct{}

const NameTheTuneFilterSongsPerArtistDefault = 20

func (_ NameTheTuneFilterSongsPerArtist) Name() string {
	return "Songs per Artist"
}

func (_ NameTheTuneFilterSongsPerArtist) Prompt() string {
	return "Maximum number of songs to consider per artist"
}

func (_ NameTheTuneFilterSongsPerArtist) Type() goshots.FilterType {
	return goshots.FilterTypeNumber
}

func (_ NameTheTuneFilterSongsPerArtist) Names(p goshots.Provider) ([]string, error) {
	return []string{}, nil
}

func (_ NameTheTuneFilterSongsPerArtist) DefaultValues() []int {
	return []int{NameTheTuneFilterSongsPerArtistDefault}
}
