package namethetune

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/garbotron/goshots2/core"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"net/url"
	"os"
	"sort"
)

type NameTheTune struct {
	log *log.Logger
}

type Tune struct {
	PreviewUrl     string
	DetailsUrl     string
	Artist         string
	Album          string
	Title          string
	ClipLengthSecs int
}

type artistJson struct {
	Id         string `json:"id"`
	Name       string `json:"name"`
	Popularity int    `json:"popularity"`
}

type artistSearchJson struct {
	Artists struct {
		Items []artistJson `json:"items"`
	} `json:"artists"`
}

type trackJson struct {
	Album struct {
		Name string `json:"name"`
	} `json:"album"`
	ExternalUrls struct {
		Spotify string `json:"spotify"`
	} `json:"external_urls"`
	Name       string `json:"name"`
	PreviewUrl string `json:"preview_url"`
}

type topTracksJson struct {
	Tracks []trackJson `json:"tracks"`
}

const (
	enableLogging = false
)

// helper to sort artist searches by popularity, descending
type artistSortByPopularityDesc []artistJson

func (a artistSortByPopularityDesc) Len() int           { return len(a) }
func (a artistSortByPopularityDesc) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a artistSortByPopularityDesc) Less(i, j int) bool { return a[i].Popularity > a[j].Popularity }

func (_ *NameTheTune) CanScrape() bool {
	return false
}

func (_ *NameTheTune) StartScraping(context goshots.ScraperContext) goshots.Scraper {
	return nil
}

func (_ *NameTheTune) ShortName() string {
	return "namethetune"
}

func (_ *NameTheTune) PrettyName() string {
	return "Name the Tune"
}

func (_ *NameTheTune) Description() []string {
	return []string{
		"Name the Tune is a drinking game for music lovers.",
		"Hear a bit of a song... can you name it?",
		"<b>If you can't get it, take a drink!</b>",
		"<b>If you can, everyone else takes a drink and you go again!</b>",
	}
}

func (_ *NameTheTune) Title() string {
	return "Name the Tune!"
}

func (_ *NameTheTune) Prompt() string {
	return "Name the tune!"
}

func (_ *NameTheTune) ElemDetailsTitle() string {
	return "Details on Spotify"
}

func (n *NameTheTune) Load() error {
	if enableLogging {
		n.log = log.New(os.Stdout, "[goshots-namethetune] ", 0)
	} else {
		n.log = log.New(ioutil.Discard, "", 0)
	}
	return nil
}

func (n *NameTheTune) RandomElem(filterValues *goshots.FilterValues) (interface{}, error) {
	filters := n.Filters()

	// Collect the filter data
	genres := []string{}
	artistsPerGenre := NameTheTuneFilterArtistsPerGenreDefault
	artistsTotal := NameTheTuneFilterArtistsTotalDefault
	songsPerArtist := NameTheTuneFilterSongsPerArtistDefault
	clipLengthSecs := NameTheTuneFilterClipLengthSecsDefault
	for i, v := range *filterValues {
		if i < len(filters) && v.Enabled && len(v.Values) >= 1 {
			if _, ok := filters[i].(NameTheTuneFilterGenre); ok {
				names, err := filters[i].Names(n)
				if err != nil {
					return nil, n.reportError("collect filters", err)
				}
				for _, i := range v.Values {
					if i < len(names) {
						genres = append(genres, names[i])
					}
				}
			}
			if _, ok := filters[i].(NameTheTuneFilterArtistsPerGenre); ok {
				artistsPerGenre = v.Values[0]
			}
			if _, ok := filters[i].(NameTheTuneFilterArtistsTotal); ok {
				artistsTotal = v.Values[0]
			}
			if _, ok := filters[i].(NameTheTuneFilterSongsPerArtist); ok {
				songsPerArtist = v.Values[0]
			}
			if _, ok := filters[i].(NameTheTuneFilterClipLengthSecs); ok {
				clipLengthSecs = v.Values[0]
			}
		}
	}

	n.log.Printf(
		"querying with %d genres, %d artists/genre, %d artists total, %d songs/artist\n",
		len(genres),
		artistsPerGenre,
		artistsTotal,
		songsPerArtist)

	// collect all lists of artists (for each genre), making sure to avoid duplicates
	artists := []artistJson{}
	artistNameMap := map[string]bool{}
	if len(genres) == 0 {
		a, err := n.queryArtistsWithoutGenre(artistsTotal)
		if err != nil {
			return nil, n.reportError("query artists", err)
		}
		artists = a
	} else {
		for _, g := range genres {
			perGenre, err := n.queryArtistsByGenre(g, artistsPerGenre)
			if err != nil {
				return nil, n.reportError("query artists", err)
			}
			for _, a := range perGenre {
				if _, inMap := artistNameMap[a.Name]; !inMap {
					artistNameMap[a.Name] = true
					artists = append(artists, a)
				}
			}
		}
	}

	// sort artists by popularity, descending
	sort.Sort(artistSortByPopularityDesc(artists))

	// trim the list to the specified maximum
	if len(artists) > artistsTotal {
		artists = append([]artistJson(nil), artists[:artistsTotal]...)
	}

	n.log.Printf("found %d artists\n", len(artists))
	if len(artists) == 0 {
		return nil, goshots.ElemNotFoundError()
	}

	// pick one of the artists at random
	artistIdx := rand.Intn(len(artists))

	n.log.Printf("chose %d -> '%s'\n", artistIdx, artists[artistIdx].Name)

	// get the list of top tracks for that artist
	tracks, err := n.queryTopTracks(artists[artistIdx].Id, songsPerArtist)
	if err != nil {
		return nil, n.reportError("query top tracks", err)
	}

	n.log.Printf("found %d tracks\n", len(tracks))

	// pick one of the tracks at random
	trackIdx := rand.Intn(len(tracks))

	n.log.Printf("chose %d -> '%s'\n", trackIdx, tracks[trackIdx].Name)

	ret := &Tune{}
	ret.PreviewUrl = tracks[trackIdx].PreviewUrl
	ret.DetailsUrl = tracks[trackIdx].ExternalUrls.Spotify
	ret.Artist = artists[artistIdx].Name
	ret.Album = tracks[trackIdx].Album.Name
	ret.Title = tracks[trackIdx].Name
	ret.ClipLengthSecs = clipLengthSecs

	return ret, nil
}

func (n *NameTheTune) ElemSolution(elem interface{}) (string, error) {
	t := elem.(*Tune)
	return fmt.Sprintf("%s - %s", t.Artist, t.Title), nil
}

func (n *NameTheTune) RenderContentHtml(elem interface{}) (string, error) {
	t := elem.(*Tune)
	audio := fmt.Sprintf(
		"<audio controls onloadedmetadata='setupAudioClip(this, %d)'><source type='audio/mpeg' src='%s'></audio>",
		t.ClipLengthSecs,
		t.PreviewUrl)
	inner := "<div style='align-self:center;'>" + audio + "</div>"
	outer := "<div style='width:100%;height:100%;display:flex;justify-content:center;'>" + inner + "</div>"
	return outer, nil
}

func (n *NameTheTune) ElemDetailsUrl(elem interface{}) (string, error) {
	t := elem.(*Tune)
	return t.DetailsUrl, nil
}

func (n *NameTheTune) queryArtistsByGenre(genre string, count int) ([]artistJson, error) {
	return n.queryArtists(fmt.Sprintf("genre:%%22%s%%22", url.QueryEscape(genre)), count)
}

func (n *NameTheTune) queryArtistsWithoutGenre(count int) ([]artistJson, error) {
	return n.queryArtists("year:0-3000", count)
}

func (n *NameTheTune) queryArtists(query string, count int) ([]artistJson, error) {
	artists := []artistJson{}
	part := &artistSearchJson{}
	for idx := 0; idx < count; idx += 50 {
		url := fmt.Sprintf("https://api.spotify.com/v1/search?q=%s&type=artist&limit=50&market=US", query)
		data, err := n.downloadPage(url)
		if err != nil {
			return []artistJson{}, n.reportError("downloadPage", err)
		}
		err = json.Unmarshal([]byte(data), part)
		if err != nil {
			return []artistJson{}, n.reportError("json.Unmarshal", err)
		}
		artists = append(artists, part.Artists.Items...)
	}
	if len(artists) > count {
		artists = append([]artistJson(nil), artists[:count]...)
	}
	return artists, nil
}

func (n *NameTheTune) queryTopTracks(artist string, count int) ([]trackJson, error) {
	tracks := []trackJson{}
	part := &topTracksJson{}
	for idx := 0; idx < count; idx += 50 {
		url := fmt.Sprintf("https://api.spotify.com/v1/artists/%s/top-tracks?country=US", artist)
		data, err := n.downloadPage(url)
		if err != nil {
			return []trackJson{}, n.reportError("downloadPage", err)
		}
		err = json.Unmarshal([]byte(data), part)
		if err != nil {
			return []trackJson{}, n.reportError("json.Unmarshal", err)
		}
		tracks = append(tracks, part.Tracks...)
	}
	if len(tracks) > count {
		tracks = append([]trackJson(nil), tracks[:count]...)
	}
	return tracks, nil
}

func (n *NameTheTune) downloadPage(url string) (string, error) {
	resp, err := http.Get(url)
	if err != nil {
		return "", n.reportError("http.Get", err)
	} else {
		defer resp.Body.Close()
		ret, err := ioutil.ReadAll(resp.Body)
		return string(ret), n.reportError("ioutil.ReadAll", err)
	}
}

func (n *NameTheTune) reportError(action string, err error) error {
	if err == nil {
		return nil
	}
	str := fmt.Sprintf("%s: %s", action, err.Error())
	n.log.Printf("ERROR: %s", str)
	return errors.New(str)
}

func (n *NameTheTune) reportErrorString(err string) error {
	n.log.Printf("ERROR: %s", err)
	return errors.New(err)
}
