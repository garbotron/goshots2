goshots
=======

Go-based engine for web drinking games.

[![build status](https://gitlab.com/garbotron/goshots/badges/master/build.svg)](https://gitlab.com/garbotron/goshots/commits/master)

No readme to speak of for now - just look at the code.  Maybe I'll write one someday.  Maybe.
