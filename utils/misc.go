package utils

import (
	"io/ioutil"
	"net"
	"net/http"
	"net/url"
	"strings"
	"time"
)

func CreateCautiousClient(proxy func(*http.Request) (*url.URL, error)) *http.Client {
	client := *http.DefaultClient
	client.Transport = &http.Transport{
		Dial: func(network, addr string) (net.Conn, error) {
			return net.DialTimeout(network, addr, time.Second*5)
		},
		Proxy: proxy,
		ResponseHeaderTimeout: time.Second * 10,
		DisableKeepAlives:     true,
	}
	client.Timeout = time.Second * 20
	return &client
}

func DownloadPage(url string, cookies ...*http.Cookie) (string, error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return "", err
	}
	for _, cookie := range cookies {
		req.AddCookie(cookie)
	}
	resp, err := CreateCautiousClient(nil).Do(req)
	if err != nil {
		return "", err
	}
	return RespToString(resp, err)
}

func DownloadPageViaPost(url string, cookies []*http.Cookie, data url.Values) (string, error) {
	req, err := http.NewRequest("POST", url, strings.NewReader(data.Encode()))
	if err != nil {
		return "", err
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	for _, cookie := range cookies {
		req.AddCookie(cookie)
	}
	resp, err := CreateCautiousClient(nil).Do(req)
	if err != nil {
		return "", err
	}
	return RespToString(resp, err)
}

func RespToString(resp *http.Response, err error) (string, error) {
	if err != nil {
		return "", err
	} else {
		defer resp.Body.Close()
		contents, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return "", err
		}
		return string(contents), nil
	}
}
